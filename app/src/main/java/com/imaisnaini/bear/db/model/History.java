package com.imaisnaini.bear.db.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.Date;
import java.util.HashMap;

@DatabaseTable(tableName = History.TBL_NAME)
public class History {
    public static final String TBL_NAME = "history";
    public static final String ID = "id";
    public static final String ID_CASE = "idCase";
    public static final String CF_USER = "cfUser";
    public static final String DATE = "historyDate";

    @DatabaseField(columnName = ID, id = true) private int id;
    @DatabaseField(columnName = ID_CASE) private int idCase;
    @DatabaseField(columnName = CF_USER) private HashMap<Integer, Float> cfUser;
    @DatabaseField(columnName = DATE) private Long historyDate;

    public History() {
    }

    public History(int idCase, HashMap<Integer, Float> cfUser, Long historyDate) {
        this.idCase = idCase;
        this.cfUser = cfUser;
        this.historyDate = historyDate;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdCase() {
        return idCase;
    }

    public void setIdCase(int idCase) {
        this.idCase = idCase;
    }

    public HashMap<Integer, Float> getCfUser() {
        return cfUser;
    }

    public void setCfUser(HashMap<Integer, Float> cfUser) {
        this.cfUser = cfUser;
    }

    public Long getHistoryDate() {
        return historyDate;
    }

    public void setHistoryDate(Long historyDate) {
        this.historyDate = historyDate;
    }
}
