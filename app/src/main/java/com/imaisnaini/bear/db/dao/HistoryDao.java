package com.imaisnaini.bear.db.dao;

import com.imaisnaini.bear.db.model.History;

public class HistoryDao extends BaseDaoCrud<History, Integer> {
    private static final HistoryDao ourInstance = new HistoryDao();

    public static HistoryDao getInstance() {
        return ourInstance;
    }

    private HistoryDao() {
    }
}
