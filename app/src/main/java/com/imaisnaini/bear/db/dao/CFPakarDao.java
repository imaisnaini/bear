package com.imaisnaini.bear.db.dao;

import com.imaisnaini.bear.db.model.CFPakar;
import com.j256.ormlite.dao.GenericRawResults;
import com.j256.ormlite.stmt.QueryBuilder;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class CFPakarDao extends BaseDaoCrud<CFPakar, Integer> {
    private static final CFPakarDao ourInstance = new CFPakarDao();

    public static CFPakarDao getInstance() {
        return ourInstance;
    }

    private CFPakarDao() {
    }

    public List<CFPakar> getByCase(int idCase) throws SQLException{
        QueryBuilder<CFPakar, Integer> queryBuilder = getDao().queryBuilder();
        queryBuilder.where().eq(CFPakar.ID_CASE, idCase);
        return getDao().query(queryBuilder.prepare());
    }

    // Mengambil daftar ID Symptom berdasarkan Case
    public List<Integer> getSymptoms(int idCase) throws SQLException{
        List<Integer> list = new ArrayList<>();
        String query = String.format("SELECT DISTINCT %s FROM %s WHERE %s == '%s'",
                CFPakar.ID_SYMPTOM,
                CFPakar.TBL_NAME,
                CFPakar.ID_CASE,
                idCase);
        GenericRawResults<String[]> rawResults = getDao().queryRaw(query);
        for (String[] results : rawResults) {
            list.add(Integer.valueOf(results[0]));
        }
        return list;
    }
}
