package com.imaisnaini.bear.db.dao;

import com.imaisnaini.bear.db.model.Symptom;

import java.sql.SQLException;
import java.util.List;

public class SymptomDao extends BaseDaoCrud<Symptom, Integer> {
    private static final SymptomDao ourInstance = new SymptomDao();

    public static SymptomDao getInstance() {
        return ourInstance;
    }

    private SymptomDao() {
    }

    public Symptom getByID(int id) throws SQLException{
        return getDao().queryForId(id);
    }
}
