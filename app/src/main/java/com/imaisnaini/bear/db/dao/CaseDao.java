package com.imaisnaini.bear.db.dao;

import com.imaisnaini.bear.db.model.Case;

import java.sql.SQLException;

public class CaseDao extends BaseDaoCrud<Case, Integer> {
    private static final CaseDao ourInstance = new CaseDao();

    public static CaseDao getInstance() {
        return ourInstance;
    }

    private CaseDao() {
    }

    public Case getByID(int id) throws SQLException{
        return getDao().queryForId(id);
    }
}
