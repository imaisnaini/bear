package com.imaisnaini.bear.db.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = Symptom.TBL_NAME)
public class Symptom {
    public static final String TBL_NAME = "symptom";
    public static final String ID = "id";
    public static final String NAME = "name";

    @DatabaseField(columnName = ID, id = true) private int id;
    @DatabaseField(columnName = NAME) private String name;

    public Symptom() {
    }

    public Symptom(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
