package com.imaisnaini.bear.db.helper;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.imaisnaini.bear.db.model.Card;
import com.imaisnaini.bear.db.model.CFPakar;
import com.imaisnaini.bear.db.model.Case;
import com.imaisnaini.bear.db.model.Coping;
import com.imaisnaini.bear.db.model.History;
import com.imaisnaini.bear.db.model.Quiz;
import com.imaisnaini.bear.db.model.Quote;
import com.imaisnaini.bear.db.model.Symptom;
import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import java.sql.SQLException;

public class DbHelper extends OrmLiteSqliteOpenHelper {
    private static final int DBVER = 1;
    public static final String DBNAME = "bear.db";

    public DbHelper(Context ctx){
        super(ctx, DBNAME, null, DBVER);
    }
    @Override
    public void onCreate(SQLiteDatabase database, ConnectionSource connectionSource) {
        try {
            TableUtils.createTable(connectionSource, Case.class);
            TableUtils.createTable(connectionSource, Symptom.class);
            TableUtils.createTable(connectionSource, Coping.class);
            TableUtils.createTable(connectionSource, CFPakar.class);
            TableUtils.createTable(connectionSource, Card.class);
            TableUtils.createTable(connectionSource, Quote.class);
            TableUtils.createTable(connectionSource, Quiz.class);
            TableUtils.createTable(connectionSource, History.class);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase database, ConnectionSource connectionSource, int oldVersion, int newVersion) {}

    @Override
    public ConnectionSource getConnectionSource() {
        return super.getConnectionSource();
    }

}