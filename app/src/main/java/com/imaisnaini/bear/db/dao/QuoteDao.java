package com.imaisnaini.bear.db.dao;

import com.imaisnaini.bear.db.model.Quote;
import com.j256.ormlite.stmt.QueryBuilder;

import java.sql.SQLException;
import java.util.List;

public class QuoteDao extends BaseDaoCrud<Quote, Integer>{
    private static final QuoteDao quoteDao = new QuoteDao();

    public static QuoteDao getInstance(){
        return quoteDao;
    }

    public List<Quote> getByCard(int idCard) throws SQLException{
        QueryBuilder<Quote, Integer> qb = getDao().queryBuilder();
        qb.where().eq(Quote.ID_CARD, idCard);
        return getDao().query(qb.prepare());
    }
}
