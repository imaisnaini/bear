package com.imaisnaini.bear.db.model;

import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.ArrayList;

@DatabaseTable(tableName = Card.TBL_NAME)
public class Card {
    public static final String TBL_NAME = "card";
    public static final String ID = "id";
    public static final String NAME = "name";
    public static final String AUTHOR = "author";

    @DatabaseField(columnName = ID, id = true) private int id;
    @DatabaseField(columnName = NAME) private String name;
    @DatabaseField(columnName = AUTHOR) private String author;

    public Card() {
    }

    public Card(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public Card(int id, String name, String author) {
        this.id = id;
        this.name = name;
        this.author = author;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }
}
