package com.imaisnaini.bear.db.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;


@DatabaseTable(tableName = Case.TBL_NAME)
public class Case {
    public static final String TBL_NAME = "case";
    public static final String ID = "id";
    public static final String NAME = "name";
    public static final String ICON = "icon";
    public static final String DESCRIPTION = "description";

    @DatabaseField(columnName =  ID, id = true)private int id;
    @DatabaseField(columnName = NAME) private String name;
    @DatabaseField(columnName = ICON) private String icon;
    @DatabaseField(columnName = DESCRIPTION) private String description;

    public Case() {
    }

    public Case(int id, String name, String icon, String description) {
        this.id = id;
        this.name = name;
        this.icon = icon;
        this.description = description;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
