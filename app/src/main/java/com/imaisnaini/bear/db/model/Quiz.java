package com.imaisnaini.bear.db.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = Quiz.TABLE_NAME)
public class Quiz {
    public static final String TABLE_NAME = "quiz";
    public static final String ID = "id";
    public static final String QUESTION = "question";
    public static final String ANSWER = "answer";
    public static final String ICON = "icon";

    @DatabaseField(columnName = ID, id = true) private int id;
    @DatabaseField(columnName = QUESTION) private String question;
    @DatabaseField(columnName = ANSWER) private int answer;
    @DatabaseField(columnName = ICON) private String icon;

    public Quiz() {
    }

    public Quiz(int id, String question, int answer, String icon) {
        this.id = id;
        this.question = question;
        this.answer = answer;
        this.icon = icon;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public int getAnswer() {
        return answer;
    }

    public void setAnswer(int answer) {
        this.answer = answer;
    }
}
