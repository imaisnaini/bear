package com.imaisnaini.bear.db.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = Quote.TABLE_NAME)
public class Quote {
    public static final String TABLE_NAME = "quotes";
    public static final String ID = "id";
    public static final String ID_CARD = "idCard";
    public static final String QUOTE = "quote";
    public static final String AUTHOR = "author";

    @DatabaseField(columnName = ID, id = true) private int id;
    @DatabaseField(columnName = ID_CARD) private int idCard;
    @DatabaseField(columnName = QUOTE) private String quote;
    @DatabaseField(columnName = AUTHOR) private String author;

    public Quote() {
    }

    public Quote(int id, int idCard, String quote, String author) {
        this.id = id;
        this.idCard = idCard;
        this.quote = quote;
        this.author = author;
    }

    public Quote(int id, int idCard, String quote) {
        this.id = id;
        this.idCard = idCard;
        this.quote = quote;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdCard() {
        return idCard;
    }

    public void setIdCard(int idCard) {
        this.idCard = idCard;
    }

    public String getQuote() {
        return quote;
    }

    public void setQuote(String quote) {
        this.quote = quote;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }
}
