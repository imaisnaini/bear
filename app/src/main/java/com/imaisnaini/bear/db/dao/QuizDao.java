package com.imaisnaini.bear.db.dao;

import com.imaisnaini.bear.db.model.Quiz;

public class QuizDao extends BaseDaoCrud<Quiz, Integer> {
    private final static QuizDao quizDao = new QuizDao();

    public static QuizDao getInstance(){
        return quizDao;
    }
}
