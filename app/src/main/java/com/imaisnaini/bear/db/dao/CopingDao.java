package com.imaisnaini.bear.db.dao;

import com.imaisnaini.bear.db.model.Coping;

import java.sql.SQLException;

public class CopingDao extends BaseDaoCrud<Coping, Integer> {
    private static final CopingDao ourInstance = new CopingDao();

    public static CopingDao getInstance() {
        return ourInstance;
    }

    private CopingDao() {
    }

    public Coping getByID(int id) throws SQLException{
        return getDao().queryForId(id);
    }
}
