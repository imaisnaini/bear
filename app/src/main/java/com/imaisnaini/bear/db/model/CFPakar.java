package com.imaisnaini.bear.db.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = CFPakar.TBL_NAME)
public class CFPakar {
    public static final String TBL_NAME = "cfpakar";
    public static final String ID = "id";
    public static final String ID_CASE = "idCase";
    public static final String ID_SYMPTOM = "idSymptom";
    public static final String ID_COPING = "idCoping";
    public static final String CF_VALUE = "cfValue";

    @DatabaseField(columnName = ID, id = true) private int id;
    @DatabaseField(columnName = ID_CASE) private int idCase;
    @DatabaseField(columnName = ID_SYMPTOM) private int idSymptom;
    @DatabaseField(columnName = ID_COPING) private int idCoping;
    @DatabaseField(columnName = CF_VALUE) private Float cfValue;

    public CFPakar() {
    }

    public CFPakar(int id, int idCase, int idSymptom, int idCoping, Float cfValue) {
        this.id = id;
        this.idCase = idCase;
        this.idSymptom = idSymptom;
        this.idCoping = idCoping;
        this.cfValue = cfValue;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdCase() {
        return idCase;
    }

    public void setIdCase(int idCase) {
        this.idCase = idCase;
    }

    public int getIdSymptom() {
        return idSymptom;
    }

    public void setIdSymptom(int idSymptom) {
        this.idSymptom = idSymptom;
    }

    public int getIdCoping() {
        return idCoping;
    }

    public void setIdCoping(int idCoping) {
        this.idCoping = idCoping;
    }

    public Float getCfValue() {
        return cfValue;
    }

    public void setCfValue(Float cfValue) {
        this.cfValue = cfValue;
    }
}
