package com.imaisnaini.bear.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.imaisnaini.bear.R;
import com.imaisnaini.bear.db.model.Coping;
import com.imaisnaini.bear.ui.presenter.CFPakarPresenter;
import com.imaisnaini.bear.ui.presenter.CopingPresenter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.zip.Inflater;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CFPakarAdapter extends RecyclerView.Adapter<CFPakarAdapter.ViewHolder> {
    Context context;
    private HashMap<Integer, Float> cfpakarList = new HashMap<>();
    private List<Coping> copingList = new ArrayList<>();
    private List<Float> valueList = new ArrayList<>();
    private CopingPresenter copingPresenter;

    public CFPakarAdapter(Context context) {
        this.context = context;
    }

    public CFPakarAdapter(Context context, HashMap<Integer, Float> cfpakarList) {
        this.context = context;
        this.cfpakarList = cfpakarList;

        for (int i : cfpakarList.keySet()){
            copingPresenter = new CopingPresenter();
            Coping coping = copingPresenter.getByID(i);
            copingList.add(coping);
            valueList.add(cfpakarList.get(i));
        }
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_cfpakar, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.tvCoping.setText(copingList.get(position).getName());
        holder.tvValue.setText(valueList.get(position).toString() + "%");
    }

    @Override
    public int getItemCount() {
        return cfpakarList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        @BindView(R.id.tvCoping_cfPakar) TextView tvCoping;
        @BindView(R.id.tvValue_cfPakar) TextView tvValue;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
