package com.imaisnaini.bear.ui.presenter;

import android.content.Context;

import com.imaisnaini.bear.db.dao.CaseDao;
import com.imaisnaini.bear.db.model.Case;
import com.imaisnaini.bear.ui.view.CaseView;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class CasePresenter {
    private CaseView mCaseView;

    public CasePresenter() {
    }

    public CasePresenter(CaseView mCaseView) {
        this.mCaseView = mCaseView;
    }

    public Case getByID(int id){
        Case cases = new Case();
        try {
            cases = CaseDao.getInstance().getByID(id);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return cases;
    }

    public void loadData(){
        mCaseView.showLoading();
        List<Case> list = new ArrayList<>();
        try {
            list = CaseDao.getInstance().read();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        mCaseView.loadCase(list);
        mCaseView.hideLoading();
    }

    public void initiateData(){
        mCaseView.showLoading();

        Case case1 = new Case(1, "Anxiety",
                "https://firebasestorage.googleapis.com/v0/b/ta-bear.appspot.com/o/anxiety.png?alt=media&token=b2d8e2d4-e446-42c3-a808-9f2c70cc975a",
                "Perasaan cemas, khawatir dan takut hal-hal buruk akan terjadi.");
        Case case2 = new Case(2, "Panic Attack",
                "https://firebasestorage.googleapis.com/v0/b/ta-bear.appspot.com/o/panic%20boy.png?alt=media&token=63ab05c9-fd93-41c2-b22a-cc119f8fbbfc",
                "Serangan panik.");
        Case case3 = new Case(3, "Self Harm Urge",
                "https://firebasestorage.googleapis.com/v0/b/ta-bear.appspot.com/o/cutting.png?alt=media&token=912a6650-2f7c-4330-be57-3ba894670232",
                "Hasrat untuk melukai diri sendiri.");

        try {
            CaseDao.getInstance().add(case1);
            CaseDao.getInstance().add(case2);
            CaseDao.getInstance().add(case3);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        mCaseView.hideLoading();
    }
}
