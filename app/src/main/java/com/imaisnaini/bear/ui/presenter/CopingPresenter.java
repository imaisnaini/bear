package com.imaisnaini.bear.ui.presenter;

import com.imaisnaini.bear.db.dao.CopingDao;
import com.imaisnaini.bear.db.model.Coping;
import com.imaisnaini.bear.ui.view.CopingView;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class CopingPresenter {
    private CopingView mCopingView;

    public CopingPresenter() {
    }

    public CopingPresenter(CopingView mCopingView) {
        this.mCopingView = mCopingView;
    }

    public Coping getByID(int id){
        Coping coping = new Coping();
        try {
            coping = CopingDao.getInstance().getByID(id);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return coping;
    }

    public void initiateData(){
        mCopingView.showLoading();
        List<Coping> list = new ArrayList<>();

        list.add(new Coping(1, "Calm Breathing"));
        list.add(new Coping(2, "Muscle Relaxation"));
        list.add(new Coping(3, "Deep Breath"));
        list.add(new Coping(4, "Finding Color"));
        list.add(new Coping(5, "Counting Object"));
        list.add(new Coping(6, "Coping Cards"));
        list.add(new Coping(7, "Grounding"));
        list.add(new Coping(8, "Worry Box"));

        for (Coping coping : list){
            try {
                CopingDao.getInstance().add(coping);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        mCopingView.hideLoading();
    }
}
