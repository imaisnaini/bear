package com.imaisnaini.bear.ui.fragment;


import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;

import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.airbnb.lottie.LottieAnimationView;
import com.imaisnaini.bear.R;
import com.imaisnaini.bear.ui.activity.MainActivity;
import com.imaisnaini.bear.ui.dialog.DialogBuilder;
import com.imaisnaini.bear.ui.util.Util;
import com.imaisnaini.bear.ui.util.Utilities;
import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * A simple {@link Fragment} subclass.
 */
public class MuscleRelaxationFragment extends Fragment {
    @BindView(R.id.ivIcon_muscleRelax)
    ImageView ivIcon;
    @BindView(R.id.lavIntro_muscleRelax)
    LottieAnimationView lavIntro;
    @BindView(R.id.tvIntroduction_muscleRelax)
    TextView tvIntro;
    @BindView(R.id.tvInstruction_muscleRelax)
    TextView tvInstruction;
    @BindView(R.id.layoutIntro_muscleRelax)
    RelativeLayout layoutIntro;
    @BindView(R.id.layoutContent_muscleRelax)
    RelativeLayout layoutContent;
    @BindView(R.id.sbAudio_muscleRelax)
    SeekBar sbAudio;
    @BindView(R.id.tvTotalDuration_muscleRelax)
    TextView tvTotalDuration;
    @BindView(R.id.tvCurrentTime_muscleRelax)
    TextView tvCurrentTime;

    private Handler mHandler;
    private MediaPlayer mpInstruction;
    private String[] instructions;
    private boolean isPlayed = false;
    private MaterialDialog mDialog;

    public MuscleRelaxationFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_muscle_relaxation, container, false);
        ButterKnife.bind(this, view);
        init();
        return view;
    }

    private void init(){
        mHandler = new Handler();
        mpInstruction = MediaPlayer.create(getActivity(), R.raw.muscle_relaxation);
        mpInstruction.setLooping(false);
        tvTotalDuration.setText(Utilities.milliSecondsToTimer(mpInstruction.getDuration()));
        tvCurrentTime.setText(Utilities.milliSecondsToTimer(mpInstruction.getCurrentPosition()));
        instructions = getResources().getStringArray(R.array.muscle_relax_instruction);
    }

    private void updateSeekBar(){
        mHandler.postDelayed(mUpdateTime, 100);
    }

    private Runnable mUpdateTime = new Runnable(){
        @Override
        public void run() {
            long totalDuration = mpInstruction.getDuration();
            long currentDuration = mpInstruction.getCurrentPosition();
            String currentTime = Utilities.milliSecondsToTimer(currentDuration);

            // Updating progress bar
            int progress = (Utilities.getProgressPercentage(currentDuration, totalDuration));
            tvCurrentTime.setText(Utilities.milliSecondsToTimer(currentDuration));
            //Log.d("Progress", ""+progress);
            sbAudio.setProgress(progress);

            // Updating text caption
            if (currentTime.equals("0:26")){
                tvInstruction.setText(instructions[1]);
            } else if (currentTime.equals("0:30")){
                tvInstruction.setText(instructions[2]);
            } else if (currentTime.equals("0:33")){
                tvInstruction.setText(instructions[3]);
            } else if (currentTime.equals("0:36")){
                tvInstruction.setText(instructions[4]);
            } else if (currentTime.equals("0:39")){
                tvInstruction.setText(instructions[5]);
            } else if (currentTime.equals("0:46")){
                tvInstruction.setText(instructions[6]);
                ivIcon.setImageResource(R.drawable.ic_human_foot);
            } else if (currentTime.equals("0:49")){
                tvInstruction.setText(instructions[7]);
            } else if (currentTime.equals("1:09")){
                tvInstruction.setText(instructions[8]);
            } else if (currentTime.equals("1:25")){
                tvInstruction.setText(instructions[9]);
            } else if (currentTime.equals("1:28")){
                tvInstruction.setText(instructions[10]);
            } else if (currentTime.equals("1:43")){
                tvInstruction.setText(instructions[11]);
            } else if (currentTime.equals("1:49")){
                tvInstruction.setText(instructions[12]);
                ivIcon.setImageResource(R.drawable.ic_human_hand);
            } else if (currentTime.equals("1:54")){
                tvInstruction.setText(instructions[13]);
            } else if (currentTime.equals("2:08")){
                tvInstruction.setText(instructions[14]);
            } else if (currentTime.equals("2:16")){
                tvInstruction.setText(instructions[15]);
            } else if (currentTime.equals("2:30")){
                tvInstruction.setText(instructions[16]);
            } else if (currentTime.equals("2:44")){
                tvInstruction.setText(instructions[17]);
            } else if (currentTime.equals("2:56")){
                tvInstruction.setText(instructions[18]);
            } else if (currentTime.equals("3:07")){
                tvInstruction.setText(instructions[19]);
                ivIcon.setImageResource(R.drawable.ic_human_head);
            } else if (currentTime.equals("3:15")){
                tvInstruction.setText(instructions[20]);
            } else if (currentTime.equals("3:28")){
                tvInstruction.setText(instructions[21]);
            } else if (currentTime.equals("3:45")){
                tvInstruction.setText(instructions[22]);
                ivIcon.setImageResource(R.drawable.ic_human_neck);
            } else if (currentTime.equals("3:57")){
                tvInstruction.setText(instructions[23]);
            } else if (currentTime.equals("4:17")){
                tvInstruction.setText(instructions[24]);
            } else if (currentTime.equals("4:20")){
                tvInstruction.setText(instructions[25]);
                ivIcon.setImageResource(R.drawable.ic_human_belly);
            } else if (currentTime.equals("4:27")){
                tvInstruction.setText(instructions[26]);
            } else if (currentTime.equals("4:42")){
                tvInstruction.setText(instructions[27]);
            } else if (currentTime.equals("4:52")){
                tvInstruction.setText(instructions[28]);
                ivIcon.setImageResource(R.drawable.ic_human);
            } else if (currentTime.equals("5:14")){
                tvInstruction.setText(instructions[29]);
            } else if (currentTime.equals("5:24")){
                tvInstruction.setText(instructions[30]);
            }

            // Running this thread after 100 milliseconds
            mHandler.postDelayed(this, 100);
        }
    };

    private void dialogFeedback(){
        mDialog = new MaterialDialog.Builder(getContext())
                .customView(R.layout.dialog_feedback, true)
                .backgroundColor(getResources().getColor(R.color.primaryLightColor))
                .build();
        TextView tvCoping = (TextView)mDialog.findViewById(R.id.tvCoping_dialogFeedback);
        CardView cvPositive = (CardView)mDialog.findViewById(R.id.cvPositive_dialogFeedback);
        CardView cvNeutral = (CardView)mDialog.findViewById(R.id.cvNeutral_dialogFeedback);
        CardView cvNegative = (CardView)mDialog.findViewById(R.id.cvNegative_dialogFeedback);

        tvCoping.setText("Muscle Relaxation");

        cvPositive.setOnClickListener(view -> {
            startActivity(new Intent(getContext(), MainActivity.class));
            getActivity().finish();
        });

        cvNeutral.setOnClickListener(view -> {
            mDialog.dismiss();
        });

        int selectedCoping = Util.getActiveCoping(getActivity());
        cvNegative.setOnClickListener(view -> {
            mDialog.dismiss();
            // check if last step
            if (selectedCoping == 0){
                startActivity(new Intent(getContext(), MainActivity.class));
                getActivity().finish();
            }else {
                getActivity().getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.fragment_container, Util.getCopingFragment(selectedCoping))
                        .commit();
            }
        });
        mDialog.show();
    }

    @OnClick(R.id.ivButtonClose_muscleRelax) void onClose(){
        DialogBuilder.closeDialog(getActivity(), (dialog, which) -> {
            startActivity(new Intent(getContext(), MainActivity.class));
            getActivity().finish();
        });
    }

    @OnClick(R.id.btnLanjut_muscleRelax) void onSwitchView(){
        layoutIntro.setVisibility(View.GONE);
        layoutContent.setVisibility(View.VISIBLE);
    }

    @OnClick(R.id.fabPlay_muscleRelax) void onPlay(){
        if (mpInstruction.isPlaying()){
            mpInstruction.pause();
        }else {
            if (isPlayed) {

            }else {
                mpInstruction.start();
                isPlayed = true;
            }
            // Updating progress bar
            updateSeekBar();
            tvInstruction.setText(instructions[0]);
        }
    }

}
