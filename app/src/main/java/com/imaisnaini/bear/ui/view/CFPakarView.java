package com.imaisnaini.bear.ui.view;

import com.imaisnaini.bear.db.model.CFPakar;

import java.util.List;

public interface CFPakarView {
    void showLoading();
    void hideLoading();
    void loadPakar(List<CFPakar> list);
}
