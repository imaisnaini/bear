package com.imaisnaini.bear.ui.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;

import com.afollestad.materialdialogs.MaterialDialog;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.imaisnaini.bear.db.dao.CardDao;
import com.imaisnaini.bear.db.dao.CFPakarDao;
import com.imaisnaini.bear.db.dao.CaseDao;
import com.imaisnaini.bear.db.dao.CopingDao;
import com.imaisnaini.bear.db.dao.QuizDao;
import com.imaisnaini.bear.db.dao.QuoteDao;
import com.imaisnaini.bear.db.dao.SymptomDao;
import com.imaisnaini.bear.db.helper.Db;
import com.imaisnaini.bear.db.model.Card;
import com.imaisnaini.bear.db.model.CFPakar;
import com.imaisnaini.bear.db.model.Case;
import com.imaisnaini.bear.db.model.Coping;
import com.imaisnaini.bear.db.model.Quiz;
import com.imaisnaini.bear.db.model.Quote;
import com.imaisnaini.bear.db.model.Symptom;
import com.imaisnaini.bear.ui.dialog.DialogAnimated;
import com.imaisnaini.bear.ui.dialog.DialogBuilder;
import com.imaisnaini.bear.ui.fragment.ExploreFragment;
import com.imaisnaini.bear.ui.fragment.HomeFragment;
import com.imaisnaini.bear.ui.fragment.JournalFragment;
import com.imaisnaini.bear.ui.fragment.ProfileFragment;
import com.imaisnaini.bear.R;
import com.imaisnaini.bear.ui.presenter.CardPresenter;
import com.imaisnaini.bear.ui.presenter.CFPakarPresenter;
import com.imaisnaini.bear.ui.presenter.CasePresenter;
import com.imaisnaini.bear.ui.presenter.CopingPresenter;
import com.imaisnaini.bear.ui.presenter.QuizPresenter;
import com.imaisnaini.bear.ui.presenter.QuotePresenter;
import com.imaisnaini.bear.ui.presenter.SymptomPresenter;
import com.imaisnaini.bear.ui.view.CCardView;
import com.imaisnaini.bear.ui.view.CFPakarView;
import com.imaisnaini.bear.ui.view.CaseView;
import com.imaisnaini.bear.ui.view.CopingView;
import com.imaisnaini.bear.ui.view.QuizView;
import com.imaisnaini.bear.ui.view.QuoteView;
import com.imaisnaini.bear.ui.view.SymptomView;

import java.sql.SQLException;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity implements CaseView, CopingView, SymptomView, CFPakarView, CCardView, QuoteView, QuizView {
    @BindView(R.id.activity_main_bottom_navigation)
    BottomNavigationView mBottomNav;

    private boolean isFirstRun = true;
    private MaterialDialog mDialog;
    private DialogAnimated mDialogAnim;
    private CasePresenter mCasePresenter;
    private CopingPresenter mCopingPresenter;
    private SymptomPresenter mSymptomPresenter;
    private CFPakarPresenter mCfPakarPresenter;
    private CardPresenter mCardPresenter;
    private QuotePresenter mQuotePresenter;
    private QuizPresenter mQuizPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        Db.getInstance().init(this);
        mBottomNav.setOnNavigationItemSelectedListener(navListener);
        initialCheck();
        initData();
        init();
    }
    // This method to initialaze view
    private void init() {
        Integer mode = getIntent().getIntExtra("mode", 0);

        mBottomNav.setOnNavigationItemSelectedListener(navListener);

        if (mode == 0) {
            mBottomNav.setSelectedItemId(R.id.nav_home);
        }else if (mode == 1){
            mBottomNav.setSelectedItemId(R.id.nav_journal);
        }else if (mode == 2){
            mBottomNav.setSelectedItemId(R.id.nav_profile);
        }
    }
    // This method to initialize data when firrst run
    private void initData(){
        mCasePresenter =  new CasePresenter(this);
        mCopingPresenter = new CopingPresenter(this);
        mSymptomPresenter = new SymptomPresenter( this);
        mCfPakarPresenter = new CFPakarPresenter(this);
        mCardPresenter = new CardPresenter(this);
        mQuotePresenter = new QuotePresenter(this);
        mQuizPresenter = new QuizPresenter(this);

        if (isFirstRun) {
            mCasePresenter.initiateData();
            mCopingPresenter.initiateData();
            mSymptomPresenter.initiateData();
            mCfPakarPresenter.initateData();
            mCardPresenter.initiateData();
            mQuotePresenter.initiateData();
            mQuizPresenter.initiateData();
        }
    }
    // This method to check if the app first run
    private void initialCheck(){
        try {
            List<Case> caseList = CaseDao.getInstance().read();
            List<Coping> copingList = CopingDao.getInstance().read();
            List<Symptom> symptomList = SymptomDao.getInstance().read();
            List<CFPakar> cfPakarList = CFPakarDao.getInstance().read();
            List<Card> cardList = CardDao.getInstance().read();
            List<Quote> quoteList = QuoteDao.getInstance().read();
            List<Quiz> quizList = QuizDao.getInstance().read();

            isFirstRun =(caseList.isEmpty() || copingList.isEmpty() || symptomList.isEmpty() ||
                    cfPakarList.isEmpty() || cardList.isEmpty() || quoteList.isEmpty() || quizList.isEmpty() || false);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        Log.i("ISFIRSTRUN", String.valueOf(isFirstRun));
    }

    private BottomNavigationView.OnNavigationItemSelectedListener navListener = item -> {
        Fragment selectedFragment = new HomeFragment();

        switch (item.getItemId()){
            case R.id.nav_home:
                selectedFragment = new HomeFragment();
                break;
            case R.id.nav_journal:
                selectedFragment = new ExploreFragment();
                break;
            case R.id.nav_profile:
                selectedFragment = new ProfileFragment();
                break;
        }
        getSupportFragmentManager().beginTransaction().replace(R.id.main_container,selectedFragment).commit();
        return true;
    };

    @Override
    public void loadPakar(List<CFPakar> list) {

    }

    @Override
    public void showLoading() {
        mDialog = DialogBuilder.showLoadingDialog(MainActivity.this, "Updating Data", "Please Wait", false);
        //mDialogAnim = new DialogAnimated();
        //mDialogAnim.show(getSupportFragmentManager(), "Loading");
    }

    @Override
    public void hideLoading() {
        mDialog.dismiss();
        //mDialogAnim.dismiss();
    }

    @Override
    public void loadQuiz(List<Quiz> list) {

    }

    @Override
    public void loadQuote(List<Quote> list) {

    }

    @Override
    public void loadCCard(List<Card> list) {

    }

    @Override
    public void loadSymptom(List<Symptom> list) {

    }

    @Override
    public void loadCoping(List<Coping> list) {

    }

    @Override
    public void loadCase(List<Case> list) {

    }
}
