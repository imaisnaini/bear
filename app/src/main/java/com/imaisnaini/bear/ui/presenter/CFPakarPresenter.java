package com.imaisnaini.bear.ui.presenter;

import com.imaisnaini.bear.db.dao.CFPakarDao;
import com.imaisnaini.bear.db.model.CFPakar;
import com.imaisnaini.bear.ui.view.CFPakarView;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class CFPakarPresenter {
    private CFPakarView mCfPakarView;

    public CFPakarPresenter() {
    }

    public CFPakarPresenter(CFPakarView mCfPakarView) {
        this.mCfPakarView = mCfPakarView;
    }

    public List<CFPakar> getByCase(int idCase){
        mCfPakarView.showLoading();
        List<CFPakar> list = new ArrayList<>();
        try {
            list = CFPakarDao.getInstance().getByCase(idCase);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        mCfPakarView.hideLoading();
        return list;
    }

    public void initateData(){
        mCfPakarView.showLoading();
        List<CFPakar> list = new ArrayList<>();

        // Anxiety - Muscle Relaxation
        list.add(new CFPakar(1, 1, 21, 2, 0.4f));
        list.add(new CFPakar(2, 1, 22, 2, 0.4f));
        list.add(new CFPakar(3, 1, 23, 2, 0.4f));
        list.add(new CFPakar(4, 1, 24, 2, 0.4f));
        list.add(new CFPakar(5, 1, 25, 2, 0f));
        list.add(new CFPakar(6, 1, 26, 2, 0.6f));
        list.add(new CFPakar(7, 1, 27, 2, 0.7f));
        list.add(new CFPakar(8, 1, 28, 2, 0f));
        list.add(new CFPakar(9, 1, 29, 2, 0f));
        list.add(new CFPakar(10, 1, 30, 2, 0f));
        list.add(new CFPakar(11, 1, 20, 2, 0f));
        list.add(new CFPakar(12, 1, 31, 2, 0.7f));
        list.add(new CFPakar(13, 1, 32, 2, 0.5f));
        list.add(new CFPakar(14, 1, 12, 2, 1f));
        list.add(new CFPakar(15, 1, 33, 2, 0.5f));
        list.add(new CFPakar(16, 1, 9, 2, 0.8f));
        list.add(new CFPakar(17, 1, 8, 2, 1f));
        list.add(new CFPakar(18, 1, 5, 2, 0.4f));
        list.add(new CFPakar(19, 1, 7, 2, 0.4f));
        list.add(new CFPakar(20, 1, 4, 2, 0.4f));
        list.add(new CFPakar(21, 1, 3, 2, 0.8f));
        list.add(new CFPakar(22, 1, 34, 2, 0.6f));
        // Anxiety - Deep Breath
        list.add(new CFPakar(23, 1, 21, 3, 0.5f));
        list.add(new CFPakar(24, 1, 22, 3, 0.5f));
        list.add(new CFPakar(25, 1, 23, 3, 0.5f));
        list.add(new CFPakar(26, 1, 24, 3, 0.5f));
        list.add(new CFPakar(27, 1, 25, 3, 0.5f));
        list.add(new CFPakar(28, 1, 26, 3, 0.5f));
        list.add(new CFPakar(29, 1, 27, 3, 0.2f));
        list.add(new CFPakar(30, 1, 28, 3, 0f));
        list.add(new CFPakar(31, 1, 29, 3, 0f));
        list.add(new CFPakar(32, 1, 30, 3, 0f));
        list.add(new CFPakar(33, 1, 20, 3, 0f));
        list.add(new CFPakar(34, 1, 31, 3, 0.7f));
        list.add(new CFPakar(35, 1, 32, 3, 0.5f));
        list.add(new CFPakar(36, 1, 12, 3, 0.6f));
        list.add(new CFPakar(37, 1, 33, 3, 0.4f));
        list.add(new CFPakar(38, 1, 9, 3, 0.4f));
        list.add(new CFPakar(39, 1, 8, 3, 0.4f));
        list.add(new CFPakar(40, 1, 5, 3, 0.7f));
        list.add(new CFPakar(41, 1, 7, 3, 1f));
        list.add(new CFPakar(42, 1, 4, 3, 0.8f));
        list.add(new CFPakar(43, 1, 3, 3, 1f));
        list.add(new CFPakar(44, 1, 34, 3, 0.3f));
        // Anxiety - Counting Object
        list.add(new CFPakar(45, 1, 21, 5, 0.8f));
        list.add(new CFPakar(46, 1, 22, 5, 0.6f));
        list.add(new CFPakar(47, 1, 23, 5, 0.8f));
        list.add(new CFPakar(48, 1, 24, 5, 1f));
        list.add(new CFPakar(49, 1, 25, 5, 0.2f));
        list.add(new CFPakar(50, 1, 26, 5, 1f));
        list.add(new CFPakar(51, 1, 27, 5, 0.6f));
        list.add(new CFPakar(52, 1, 28, 5, 0f));
        list.add(new CFPakar(53, 1, 29, 5, 0.3f));
        list.add(new CFPakar(54, 1, 30, 5, 0f));
        list.add(new CFPakar(55, 1, 20, 5, 0f));
        list.add(new CFPakar(56, 1, 31, 5, 0.7f));
        list.add(new CFPakar(57, 1, 32, 5, 0.6f));
        list.add(new CFPakar(58, 1, 12, 5, 0.4f));
        list.add(new CFPakar(59, 1, 33, 5, 0f));
        list.add(new CFPakar(60, 1, 9, 5, 0.3f));
        list.add(new CFPakar(61, 1, 8, 5, 0f));
        list.add(new CFPakar(62, 1, 5, 5, 0.2f));
        list.add(new CFPakar(63, 1, 7, 5, 0.5f));
        list.add(new CFPakar(64, 1, 4, 5, 0.5f));
        list.add(new CFPakar(65, 1, 3, 5, 0.5f));
        list.add(new CFPakar(66, 1, 34, 5, 1f));
        // Anxiety - Coping Cards
        list.add(new CFPakar(67, 1, 21, 6, 0.6f));
        list.add(new CFPakar(68, 1, 22, 6, 0.6f));
        list.add(new CFPakar(69, 1, 23, 6, 0.6f));
        list.add(new CFPakar(70, 1, 24, 6, 0.6f));
        list.add(new CFPakar(71, 1, 25, 6, 0.4f));
        list.add(new CFPakar(72, 1, 26, 6, 0.8f));
        list.add(new CFPakar(73, 1, 27, 6, 0.8f));
        list.add(new CFPakar(74, 1, 28, 6, 1f));
        list.add(new CFPakar(75, 1, 29, 6, 1f));
        list.add(new CFPakar(76, 1, 30, 6, 1f));
        list.add(new CFPakar(77, 1, 20, 6, 0.8f));
        list.add(new CFPakar(78, 1, 31, 6, 0.3f));
        list.add(new CFPakar(79, 1, 32, 6, 0.3f));
        list.add(new CFPakar(80, 1, 12, 6, 0f));
        list.add(new CFPakar(81, 1, 33, 6, 0f));
        list.add(new CFPakar(82, 1, 9, 6, 0f));
        list.add(new CFPakar(83, 1, 8, 6, 0f));
        list.add(new CFPakar(84, 1, 5, 6, 0.2f));
        list.add(new CFPakar(85, 1, 7, 6, 0.3f));
        list.add(new CFPakar(86, 1, 4, 6, 0.3f));
        list.add(new CFPakar(87, 1, 3, 6, 0.3f));
        list.add(new CFPakar(88, 1, 34, 6, 1f));
        // Anxiety - Grounding
        list.add(new CFPakar(89, 1, 21, 7, 0.7f));
        list.add(new CFPakar(90, 1, 22, 7, 0.7f));
        list.add(new CFPakar(91, 1, 23, 7, 0.7f));
        list.add(new CFPakar(92, 1, 24, 7, 1f));
        list.add(new CFPakar(93, 1, 25, 7, 0.7f));
        list.add(new CFPakar(94, 1, 26, 7, 1f));
        list.add(new CFPakar(95, 1, 27, 7, 0.8f));
        list.add(new CFPakar(96, 1, 28, 7, 0.4f));
        list.add(new CFPakar(97, 1, 29, 7, 0.6f));
        list.add(new CFPakar(98, 1, 30, 7, 0.3f));
        list.add(new CFPakar(99, 1, 20, 7, 0f));
        list.add(new CFPakar(100, 1, 31, 7, 0.5f));
        list.add(new CFPakar(101, 1, 32, 7, 0.4f));
        list.add(new CFPakar(102, 1, 12, 7, 0.5f));
        list.add(new CFPakar(103, 1, 33, 7, 0f));
        list.add(new CFPakar(104, 1, 9, 7, 0f));
        list.add(new CFPakar(105, 1, 8, 7, 0f));
        list.add(new CFPakar(106, 1, 5, 7, 0.2f));
        list.add(new CFPakar(107, 1, 7, 7, 0.3f));
        list.add(new CFPakar(108, 1, 4, 7, 0.3f));
        list.add(new CFPakar(109, 1, 3, 7, 0.3f));
        list.add(new CFPakar(110, 1, 34, 7, 0.6f));
        // Anxiety - Worry Box
        list.add(new CFPakar(111, 1, 21, 8, 0.8f));
        list.add(new CFPakar(112, 1, 22, 8, 0.8f));
        list.add(new CFPakar(113, 1, 23, 8, 0.8f));
        list.add(new CFPakar(114, 1, 24, 8, 0.6f));
        list.add(new CFPakar(115, 1, 25, 8, 0.6f));
        list.add(new CFPakar(116, 1, 26, 8, 1f));
        list.add(new CFPakar(117, 1, 27, 8, 0.8f));
        list.add(new CFPakar(118, 1, 28, 8, 0.6f));
        list.add(new CFPakar(119, 1, 29, 8, 0.8f));
        list.add(new CFPakar(120, 1, 30, 8, 0.7f));
        list.add(new CFPakar(121, 1, 20, 8, 0.7f));
        list.add(new CFPakar(122, 1, 31, 8, 0.3f));
        list.add(new CFPakar(123, 1, 32, 8, 0.3f));
        list.add(new CFPakar(124, 1, 12, 8, 0f));
        list.add(new CFPakar(125, 1, 33, 8, 0f));
        list.add(new CFPakar(126, 1, 9, 8, 0f));
        list.add(new CFPakar(127, 1, 8, 8, 0f));
        list.add(new CFPakar(128, 1, 5, 8, 0.2f));
        list.add(new CFPakar(129, 1, 7, 8, 0.3f));
        list.add(new CFPakar(130, 1, 4, 8, 0.3f));
        list.add(new CFPakar(131, 1, 3, 8, 0.3f));
        list.add(new CFPakar(132, 1, 34, 8, 0.1f));
        // Panic - Muscle Relaxation
        list.add(new CFPakar(133, 2, 1, 2, 0.5f));
        list.add(new CFPakar(134, 2, 2, 2, 0.3f));
        list.add(new CFPakar(135, 2, 3, 2, 0.8f));
        list.add(new CFPakar(136, 2, 4, 2, 0.4f));
        list.add(new CFPakar(137, 2, 5, 2, 0.4f));
        list.add(new CFPakar(138, 2, 6, 2, 0.8f));
        list.add(new CFPakar(139, 2, 7, 2, 0.4f));
        list.add(new CFPakar(140, 2, 8, 2, 1f));
        list.add(new CFPakar(141, 2, 9, 2, 1f));
        list.add(new CFPakar(142, 2, 10, 2, 1f));
        list.add(new CFPakar(143, 2, 11, 2, 0.8f));
        list.add(new CFPakar(144, 2, 12, 2, 1f));
        list.add(new CFPakar(145, 2, 13, 2, 0.3f));
        list.add(new CFPakar(146, 2, 14, 2, 0.6f));
        list.add(new CFPakar(147, 2, 15, 2, 0f));
        list.add(new CFPakar(148, 2, 16, 2, 0f));
        list.add(new CFPakar(149, 2, 17, 2, 0.4f));
        list.add(new CFPakar(150, 2, 18, 2, 0f));
        list.add(new CFPakar(151, 2, 19, 2, 0f));
        list.add(new CFPakar(152, 2, 20, 2, 0.3f));
        // Panic - Calm Breathing
        list.add(new CFPakar(153, 2, 1, 2, 0.5f));
        list.add(new CFPakar(154, 2, 2, 2, 0.3f));
        list.add(new CFPakar(155, 2, 3, 2, 0.8f));
        list.add(new CFPakar(156, 2, 4, 2, 0.4f));
        list.add(new CFPakar(157, 2, 5, 2, 0.4f));
        list.add(new CFPakar(158, 2, 6, 2, 0.8f));
        list.add(new CFPakar(159, 2, 7, 2, 0.4f));
        list.add(new CFPakar(160, 2, 8, 2, 1f));
        list.add(new CFPakar(161, 2, 9, 2, 1f));
        list.add(new CFPakar(162, 2, 10, 2, 1f));
        list.add(new CFPakar(163, 2, 11, 2, 0.8f));
        list.add(new CFPakar(164, 2, 12, 2, 1f));
        list.add(new CFPakar(165, 2, 13, 2, 0.3f));
        list.add(new CFPakar(166, 2, 14, 2, 0.6f));
        list.add(new CFPakar(167, 2, 15, 2, 0f));
        list.add(new CFPakar(168, 2, 16, 2, 0f));
        list.add(new CFPakar(169, 2, 17, 2, 0.4f));
        list.add(new CFPakar(170, 2, 18, 2, 0f));
        list.add(new CFPakar(171, 2, 19, 2, 0f));
        list.add(new CFPakar(172, 2, 20, 2, 0.3f));
        // Panic - Grounding
        list.add(new CFPakar(173, 2, 1, 7, 0.3f));
        list.add(new CFPakar(174, 2, 2, 7, 0.3f));
        list.add(new CFPakar(175, 2, 3, 7, 0.8f));
        list.add(new CFPakar(176, 2, 4, 7, 0.8f));
        list.add(new CFPakar(177, 2, 5, 7, 0.6f));
        list.add(new CFPakar(178, 2, 6, 7, 0.3f));
        list.add(new CFPakar(179, 2, 7, 7, 0.5f));
        list.add(new CFPakar(180, 2, 8, 7, 0f));
        list.add(new CFPakar(181, 2, 9, 7, 0f));
        list.add(new CFPakar(182, 2, 10, 7, 0f));
        list.add(new CFPakar(183, 2, 11, 7, 0.4f));
        list.add(new CFPakar(184, 2, 12, 7, 0f));
        list.add(new CFPakar(185, 2, 13, 7, 0f));
        list.add(new CFPakar(186, 2, 14, 7, 0.5f));
        list.add(new CFPakar(187, 2, 15, 7, 0.8f));
        list.add(new CFPakar(188, 2, 16, 7, 1f));
        list.add(new CFPakar(189, 2, 17, 7, 1f));
        list.add(new CFPakar(190, 2, 18, 7, 0.9f));
        list.add(new CFPakar(191, 2, 19, 7, 1f));
        list.add(new CFPakar(192, 2, 20, 7, 0.8f));
        // Panic - Coping Cards
        list.add(new CFPakar(173, 2, 1, 6, 0.6f));
        list.add(new CFPakar(174, 2, 2, 6, 0.4f));
        list.add(new CFPakar(175, 2, 3, 6, 0.6f));
        list.add(new CFPakar(176, 2, 4, 6, 0.7f));
        list.add(new CFPakar(177, 2, 5, 6, 0.2f));
        list.add(new CFPakar(178, 2, 6, 6, 0.2f));
        list.add(new CFPakar(179, 2, 7, 6, 0.4f));
        list.add(new CFPakar(180, 2, 8, 6, 0f));
        list.add(new CFPakar(181, 2, 9, 6, 0f));
        list.add(new CFPakar(182, 2, 10, 6, 0f));
        list.add(new CFPakar(183, 2, 11, 6, 0.4f));
        list.add(new CFPakar(184, 2, 12, 6, 0f));
        list.add(new CFPakar(185, 2, 13, 6, 0.2f));
        list.add(new CFPakar(186, 2, 14, 6, 0.5f));
        list.add(new CFPakar(187, 2, 15, 6, 1f));
        list.add(new CFPakar(188, 2, 16, 6, 1f));
        list.add(new CFPakar(189, 2, 17, 6, 1f));
        list.add(new CFPakar(190, 2, 18, 6, 1f));
        list.add(new CFPakar(191, 2, 19, 6, 1f));
        list.add(new CFPakar(192, 2, 20, 6, 0.8f));

        for (CFPakar pakar : list){
            try {
                CFPakarDao.getInstance().add(pakar);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        mCfPakarView.hideLoading();
    }
}
