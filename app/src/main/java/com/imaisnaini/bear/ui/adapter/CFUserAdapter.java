package com.imaisnaini.bear.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.imaisnaini.bear.R;
import com.imaisnaini.bear.db.model.Symptom;
import com.imaisnaini.bear.ui.presenter.SymptomPresenter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CFUserAdapter extends RecyclerView.Adapter<CFUserAdapter.ViewHolder> {
    Context context;
    private SymptomPresenter symptomPresenter;
    private HashMap<Integer, Float> listCFUser = new HashMap<>();
    private List<Symptom> symptomList = new ArrayList<>();
    private List<Float> valueList = new ArrayList<>();

    public CFUserAdapter(Context context) {
        this.context = context;
    }

    public CFUserAdapter(Context context, HashMap<Integer, Float> listCFUser) {
        this.context = context;
        this.listCFUser = listCFUser;
        symptomPresenter = new SymptomPresenter();

        for (int i : listCFUser.keySet()){
            Symptom symptom = symptomPresenter.getByID(i);
            symptomList.add(symptom);
            valueList.add(listCFUser.get(i));
        }
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_cfuser, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.tvSymptom.setText(symptomList.get(position).getName());
        holder.tvValue.setText(valueList.get(position).toString());
    }

    @Override
    public int getItemCount() {
        return listCFUser.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        @BindView(R.id.tvSymptom_cfUser) TextView tvSymptom;
        @BindView(R.id.tvValue_cfUser) TextView tvValue;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
