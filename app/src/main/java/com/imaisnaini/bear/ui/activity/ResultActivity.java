package com.imaisnaini.bear.ui.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.afollestad.materialdialogs.MaterialDialog;
import com.imaisnaini.bear.R;
import com.imaisnaini.bear.db.model.CFPakar;
import com.imaisnaini.bear.db.model.Coping;
import com.imaisnaini.bear.db.model.History;
import com.imaisnaini.bear.ui.adapter.CFPakarAdapter;
import com.imaisnaini.bear.ui.adapter.CFUserAdapter;
import com.imaisnaini.bear.ui.dialog.DialogBuilder;
import com.imaisnaini.bear.ui.presenter.CFPakarPresenter;
import com.imaisnaini.bear.ui.presenter.HistoryPresenter;
import com.imaisnaini.bear.ui.util.PrefUtil;
import com.imaisnaini.bear.ui.util.Util;
import com.imaisnaini.bear.ui.view.CFPakarView;
import com.imaisnaini.bear.ui.view.HistoryView;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.internal.Utils;

public class ResultActivity extends AppCompatActivity implements HistoryView, CFPakarView {
    @BindView(R.id.rvHasilCF_result)
    RecyclerView rvCFHasil;
    @BindView(R.id.rvUser_result)
    RecyclerView rvCFUser;
    @BindView(R.id.toolbar)
    Toolbar toolbar;

    private List<CFPakar> cfPakarList = new ArrayList<>();
    private HashMap<Integer, Float> cfUser = new HashMap<>();
    private HashMap<Integer, Float> cfFinalResult = new HashMap<>();
    private List<Float> cfHasilCoping = new ArrayList<>();
    private List<Integer> listIDCoping = new ArrayList<>();
    private int[] listID;
    private float[] listCF;
    private int idCase;
    private HistoryPresenter mHistoryPresenter;
    private CFPakarPresenter mCfPakarPresenter;
    private CFUserAdapter mCfUserAdapter;
    private CFPakarAdapter mCfPakarAdapter;
    private MaterialDialog materialDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);
        ButterKnife.bind(this);
        initData();
        calculateCF();
        initView();
    }

    private void initData(){
        Bundle bundle = this.getIntent().getExtras();
        idCase = bundle.getInt("idCase");
        listID = bundle.getIntArray("listID");
        listCF = bundle.getFloatArray("listCF");

        for (int i = 0; i < listID.length; i++){
            cfUser.put(listID[i], listCF[i]);
        }

        mCfPakarPresenter = new CFPakarPresenter(this);
        mHistoryPresenter = new HistoryPresenter(this);
        mHistoryPresenter.add(idCase, cfUser);
        cfPakarList = mCfPakarPresenter.getByCase(idCase);
    }

    private void initView(){
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        mCfUserAdapter = new CFUserAdapter(getApplicationContext(), cfUser);
        mCfPakarAdapter =  new CFPakarAdapter(getApplicationContext(), cfFinalResult);

        rvCFUser.setHasFixedSize(true);
        rvCFUser.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        rvCFUser.setAdapter(mCfUserAdapter);

        rvCFHasil.setHasFixedSize(true);
        rvCFHasil.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        rvCFHasil.setAdapter(mCfPakarAdapter);
    }

    private void calculateCF(){
        materialDialog = DialogBuilder.showLoadingDialog(ResultActivity.this, "Calculating", "Please wait, the system is calculating your result", false);
        materialDialog.show();
        List<CFPakar> listByCoping = new ArrayList<>();
        listIDCoping = listIDCoping();

        for (int idCoping : listIDCoping){
            listByCoping.clear();
            listByCoping = listByCoping(idCoping);
            CFHasil(listByCoping, cfUser);

            float cfCombine = CFCombine(cfHasilCoping.get(0), cfHasilCoping.get(1));
            for (int i = 2; i < listByCoping.size(); i++){
                cfCombine = CFCombine(cfCombine, cfHasilCoping.get(i));
            }
            cfFinalResult.put(idCoping, cfCombine*100);
        }
        storeToPref();
        materialDialog.dismiss();
        startActivity(new Intent(ResultActivity.this, CopingActivity.class));
        finish();
    }

    private void storeToPref(){
        HashSet<String> cfSet = new HashSet<>();
        for (int key : cfFinalResult.keySet()){
            cfSet.add(key + "-" + cfFinalResult.get(key));
        }
        PrefUtil.putStringSet(getApplication(), PrefUtil.COPING_SESSION, cfSet);
        PrefUtil.putString(getApplication(), PrefUtil.COPING_STEP, String.valueOf(0));
        Log.i("COPING_SESSION", PrefUtil.getStringSet(getApplication(), PrefUtil.COPING_SESSION).toString());
        Log.i("COPING_STEP", PrefUtil.getString(getApplication(), PrefUtil.COPING_STEP));
    }

    // mendapatkan daftar coping dari list CFPakar
    private List<Integer> listIDCoping(){
        List<Integer> list = new ArrayList<>();
        for (CFPakar pakar : cfPakarList){
            if (!list.contains(pakar.getIdCoping())){
                list.add(pakar.getIdCoping());
            }
        }
        return list;
    }

    // membuat list baru berdasarkan coping
    private List<CFPakar> listByCoping(int idCoping){
        List<CFPakar> list = new ArrayList<>();
        for (CFPakar pakar : cfPakarList){
            if (pakar.getIdCoping() == idCoping){
                list.add(pakar);
            }
        }
        return list;
    }

    // Menghitung CFHasil = (CFRule * CFUser) berdasarkan coping
    private void CFHasil(List<CFPakar> pakarList, HashMap<Integer, Float> cfUser){
        float cfHasil = 0f;
        cfHasilCoping.clear();
        for (CFPakar pakar : pakarList){
            float cfUserValue = 0f;
            if (cfUser.containsKey(pakar.getIdSymptom())){
                cfUserValue = cfUser.get(pakar.getIdSymptom());
            }
            cfHasil = pakar.getCfValue() * cfUserValue;
            cfHasilCoping.add(cfHasil);
        }
    }

    // rumus CFCombine, input dari CFHasil = (CFRule * CFUser)
    private float CFCombine(float cf1, float cf2){
        float cfCombine = cf1 + cf2 * (1 - cf1);
        return cfCombine;
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();

    }

    @Override
    public void showLoading() {

    }

    @Override
    public void hideLoading() {

    }

    @Override
    public void loadPakar(List<CFPakar> list) {
    }

    @Override
    public void loadHistory(List<History> historyList) {

    }
}
