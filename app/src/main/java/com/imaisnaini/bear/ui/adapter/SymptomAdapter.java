package com.imaisnaini.bear.ui.adapter;

import android.content.Context;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.SeekBar;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.imaisnaini.bear.R;
import com.imaisnaini.bear.db.model.History;
import com.imaisnaini.bear.db.model.Symptom;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SymptomAdapter extends RecyclerView.Adapter<SymptomAdapter.ViewHolder> {
    private Context context;
    private List<Symptom> mSymptomList =  new ArrayList<>();
    private HashMap<Integer, Float> cfUser = new HashMap<>();

    public SymptomAdapter(Context context) {
        this.context = context;
    }

    public SymptomAdapter(Context context, List<Symptom> mSymptomList) {
        this.context = context;
        this.mSymptomList = mSymptomList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view =   LayoutInflater.from(parent.getContext()).inflate(R.layout.item_symptom, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Symptom symptom = mSymptomList.get(position);
        holder.cbSymptom.setText(symptom.getName());
        holder.cbSymptom.setOnCheckedChangeListener((compoundButton, isChecked) -> {
            if (isChecked){
                int progress = holder.sbCFSymptom.getProgress();
                Float value = 0f;
                switch (progress){
                    case 0:
                        value = 0.33f;
                        break;
                    case 1:
                        value = 0.66f;
                        break;
                    case 2:
                        value = 0.99f;
                        break;
                    default:
                        value = 0f;
                        break;

                }
                holder.layoutSeekbar.setVisibility(View.VISIBLE);
                cfUser.put(symptom.getId(), value);
            }else {
                holder.layoutSeekbar.setVisibility(View.GONE);
                cfUser.remove(symptom.getId());
            }
        });
        holder.sbCFSymptom.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean b) {
                Float value = 0f;
                switch (progress){
                    case 0:
                        value = 0.25f;
                        break;
                    case 1:
                        value = 0.5f;
                        break;
                    case 2:
                        value = 0.75f;
                        break;
                    default:
                        value = 0f;
                        break;

                }
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    cfUser.replace(symptom.getId(), value);
                }else {
                    cfUser.remove(symptom.getId());
                    cfUser.put(symptom.getId(), value);
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
    }

    @Override
    public int getItemCount() {
        return mSymptomList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        @BindView(R.id.cbSymptom)
        CheckBox cbSymptom;
        @BindView(R.id.sbCFSymptom)
        SeekBar sbCFSymptom;
        @BindView(R.id.layoutSBSymptom)
        LinearLayout layoutSeekbar;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public void generate(List<Symptom> list){
        clear();
        this.mSymptomList = list;
        notifyDataSetChanged();
    }

    public void clear(){
        this.mSymptomList.clear();
        notifyDataSetChanged();
    }

    public HashMap<Integer, Float> listCFUser(){
        return cfUser;
    }
}
