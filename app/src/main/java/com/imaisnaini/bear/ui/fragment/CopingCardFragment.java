package com.imaisnaini.bear.ui.fragment;


import android.content.Intent;
import android.os.Bundle;

import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.LinearInterpolator;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.imaisnaini.bear.R;
import com.imaisnaini.bear.db.model.Card;
import com.imaisnaini.bear.db.model.Quote;
import com.imaisnaini.bear.event.CardEvent;
import com.imaisnaini.bear.ui.activity.MainActivity;
import com.imaisnaini.bear.ui.adapter.CardMenuAdapter;
import com.imaisnaini.bear.ui.adapter.CardQuoteAdapter;
import com.imaisnaini.bear.ui.dialog.DialogBuilder;
import com.imaisnaini.bear.ui.presenter.CardPresenter;
import com.imaisnaini.bear.ui.presenter.QuotePresenter;
import com.imaisnaini.bear.ui.util.Util;
import com.imaisnaini.bear.ui.view.CCardView;
import com.yuyakaido.android.cardstackview.CardStackLayoutManager;
import com.yuyakaido.android.cardstackview.CardStackListener;
import com.yuyakaido.android.cardstackview.CardStackView;
import com.yuyakaido.android.cardstackview.Direction;
import com.yuyakaido.android.cardstackview.Duration;
import com.yuyakaido.android.cardstackview.StackFrom;
import com.yuyakaido.android.cardstackview.SwipeAnimationSetting;
import com.yuyakaido.android.cardstackview.SwipeableMethod;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * A simple {@link Fragment} subclass.
 */
public class CopingCardFragment extends Fragment implements CCardView, CardStackListener {
    @BindView(R.id.csvQuote_copingCard)
    CardStackView csvQuote;
    @BindView(R.id.layoutSwipe_copingCard)
    RelativeLayout layoutSwipe;
    @BindView(R.id.layoutMenu_copingCard)
    LinearLayout layoutMenu;
    @BindView(R.id.rvMenu_copingCard)
    RecyclerView rvMenu;

    private CardPresenter mCardPresenter;
    private QuotePresenter mQuotePresenter;
    private List<Card> mCardList = new ArrayList<>();
    private List<Quote> mQuoteList = new ArrayList<>();
    private CardMenuAdapter mMenuAdapter;
    private CardQuoteAdapter mQuoteAdapter;
    private CardStackLayoutManager mCardManager;
    private MaterialDialog mDialog;
    private int count;

    public CopingCardFragment() {
        // Required empty public constructor
    }

    @Subscribe(threadMode = ThreadMode.MAIN) public void setCard(CardEvent event){
        mQuoteList = mQuotePresenter.getByCard(event.idCard);
        count = mQuoteList.size();
        initQuote();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragmentAZ$%
        View view = inflater.inflate(R.layout.fragment_coping_card, container, false);
        ButterKnife.bind(this, view);
        EventBus.getDefault().register(this);
        init();
        return view;
    }

    private void init(){
        mCardPresenter = new CardPresenter(this);
        mQuotePresenter = new QuotePresenter();
        mCardList = mCardPresenter.getList();
        mMenuAdapter = new CardMenuAdapter(getActivity(), mCardList);
        rvMenu.setLayoutManager( new LinearLayoutManager(getActivity()));
        rvMenu.setAdapter(mMenuAdapter);
    }

    private void reinit(){
        layoutMenu.setVisibility(View.VISIBLE);
        layoutSwipe.setVisibility(View.GONE);
    }

    private void initQuote(){
        layoutMenu.setVisibility(View.GONE);
        layoutSwipe.setVisibility(View.VISIBLE);

        mQuoteAdapter = new CardQuoteAdapter(getActivity(), mQuoteList);
        mCardManager = new CardStackLayoutManager(getContext(), this);

        mCardManager.setStackFrom(StackFrom.None);
        mCardManager.setVisibleCount(3);
        mCardManager.setTranslationInterval(8.0f);
        mCardManager.setScaleInterval(0.95f);
        mCardManager.setSwipeThreshold(0.3f);
        mCardManager.setMaxDegree(20.0f);
        mCardManager.setDirections(Direction.HORIZONTAL);
        mCardManager.setCanScrollHorizontal(true);
        mCardManager.setCanScrollVertical(true);
        mCardManager.setSwipeableMethod(SwipeableMethod.AutomaticAndManual);
        mCardManager.setOverlayInterpolator(new LinearInterpolator());

        csvQuote.setLayoutManager(mCardManager);
        csvQuote.setAdapter(mQuoteAdapter);
    }

    private void paginate(){
        // TODO : pagination for card stack
    }

    private void dialogFeedback(){
        mDialog = new MaterialDialog.Builder(getContext())
                .customView(R.layout.dialog_feedback, true)
                .backgroundColor(getResources().getColor(R.color.primaryLightColor))
                .build();

        TextView tvCoping = (TextView)mDialog.findViewById(R.id.tvCoping_dialogFeedback);
        CardView cvPositive = (CardView)mDialog.findViewById(R.id.cvPositive_dialogFeedback);
        CardView cvNeutral = (CardView)mDialog.findViewById(R.id.cvNeutral_dialogFeedback);
        CardView cvNegative = (CardView)mDialog.findViewById(R.id.cvNegative_dialogFeedback);

        tvCoping.setText("Coping Card");

        cvPositive.setOnClickListener(view -> {
            startActivity(new Intent(getContext(), MainActivity.class));
            getActivity().finish();
        });

        cvNeutral.setOnClickListener(view -> {
            mDialog.dismiss();
            reinit();
        });

        int selectedCoping = Util.getActiveCoping(getActivity());
        cvNegative.setOnClickListener(view -> {
            mDialog.dismiss();
            // check if last step
            if (selectedCoping == 0){
                startActivity(new Intent(getContext(), MainActivity.class));
                getActivity().finish();
            }else {
                getActivity().getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.fragment_container, Util.getCopingFragment(selectedCoping))
                        .commit();
            }
        });
        mDialog.show();
    }

    @OnClick(R.id.ivButtonClose_copingCard) void onClose(){
        DialogBuilder.closeDialog(getActivity(), (dialog, which) -> {
            startActivity(new Intent(getContext(), MainActivity.class));
            getActivity().finish();
        });
    }

    @OnClick(R.id.fabLike_copingCard) void onLike(){
        SwipeAnimationSetting setting = new SwipeAnimationSetting.Builder()
                .setDirection(Direction.Right)
                .setDuration(Duration.Normal.duration)
                .setInterpolator(new AccelerateInterpolator())
                .build();
        mCardManager.setSwipeAnimationSetting(setting);
        csvQuote.swipe();
    }

    @OnClick(R.id.fabDislike_copingCard) void onDislike(){
        SwipeAnimationSetting setting = new SwipeAnimationSetting.Builder()
                .setDirection(Direction.Left)
                .setDuration(Duration.Normal.duration)
                .setInterpolator(new AccelerateInterpolator())
                .build();
        mCardManager.setSwipeAnimationSetting(setting);
        csvQuote.swipe();
    }

    @Override
    public void showLoading() {

    }

    @Override
    public void hideLoading() {

    }

    @Override
    public void loadCCard(List<Card> list) {

    }

    @Override
    public void onCardDragging(Direction direction, float ratio) {

    }

    @Override
    public void onCardSwiped(Direction direction) {
        count -= 1;
        if (count == 0){
            new Handler().postDelayed(() -> {
                dialogFeedback();
            }, 2000);
        }
    }

    @Override
    public void onCardRewound() {

    }

    @Override
    public void onCardCanceled() {

    }

    @Override
    public void onCardAppeared(View view, int position) {

    }

    @Override
    public void onCardDisappeared(View view, int position) {

    }
}
