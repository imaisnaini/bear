package com.imaisnaini.bear.ui.view;

import com.imaisnaini.bear.db.model.Quiz;

import java.util.List;

public interface QuizView {
    void showLoading();
    void hideLoading();
    void loadQuiz(List<Quiz> list);
}
