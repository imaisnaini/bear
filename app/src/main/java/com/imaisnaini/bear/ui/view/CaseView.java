package com.imaisnaini.bear.ui.view;

import com.imaisnaini.bear.db.model.Case;

import java.util.List;

public interface CaseView {
    void showLoading();
    void hideLoading();
    void loadCase(List<Case> list);
}
