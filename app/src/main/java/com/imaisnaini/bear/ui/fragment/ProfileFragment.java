package com.imaisnaini.bear.ui.fragment;


import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.imaisnaini.bear.R;
import com.imaisnaini.bear.ui.dialog.DialogBuilder;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


/**
 * A simple {@link Fragment} subclass.
 */
public class ProfileFragment extends Fragment {
    @BindView(R.id.profile_tvUserName)
    TextView tvUsername;
    @BindView(R.id.profile_tvIdentity)
    TextView tvIdentity;

    private FirebaseUser mUser;
    private FirebaseAuth mAuth;

    public ProfileFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_profile, container, false);
        ButterKnife.bind(this, view);
        init();
        return view;
    }

    private void init(){
        mAuth = FirebaseAuth.getInstance();
        mUser = mAuth.getCurrentUser();

        tvUsername.setText(mUser.getDisplayName());
        tvIdentity.setText(mUser.getEmail());
    }

    @OnClick({R.id.profile_rlDaftarFav, R.id.profile_rlCitra}) void onClickFav(){
        DialogBuilder.alertDialog(getContext(), "Bear", "Maaf fitur belum tersedia");
    }
}
