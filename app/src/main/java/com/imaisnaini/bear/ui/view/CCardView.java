package com.imaisnaini.bear.ui.view;

import com.imaisnaini.bear.db.model.Card;

import java.util.List;

public interface CCardView {
    void showLoading();
    void hideLoading();
    void loadCCard(List<Card> list);
}
