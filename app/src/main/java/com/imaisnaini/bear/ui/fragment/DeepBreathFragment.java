package com.imaisnaini.bear.ui.fragment;


import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;

import android.os.CountDownTimer;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.imaisnaini.bear.R;
import com.imaisnaini.bear.ui.activity.MainActivity;
import com.imaisnaini.bear.ui.dialog.DialogBuilder;
import com.imaisnaini.bear.ui.util.Util;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * A simple {@link Fragment} subclass.
 */
public class DeepBreathFragment extends Fragment {
    @BindView(R.id.ivIcon_deepBreath)
    ImageView ivIcon;
    @BindView(R.id.tvInstruction_deepBreath)
    TextView tvInstruction;
    @BindView(R.id.progressBar_deepBreath)
    ProgressBar progressBar;
    @BindView(R.id.ivButtonClose_deepBreath)
    ImageView btnClose;
    @BindView(R.id.btnAction_deepBreath)
    Button btnAction;

    private MaterialDialog mDialog;
    private CountDownTimer mTimer;
    private ObjectAnimator progressAnimator;
    private boolean flag = false;
    private boolean isPaused = false;
    private int i = 0;
    private int mode = 0;

    public DeepBreathFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_deep_breath, container, false);
        ButterKnife.bind(this, view);
        initViews();
        return view;
    }

    private void initViews(){
        Picasso.get().load(R.drawable.ic_inhale).into(ivIcon);
        tvInstruction.setText("Sebelum mulai, posisikan tubuh untuk relax. Kamu bisa berdiri, duduk ataupun berbaring.");

        progressAnimator = ObjectAnimator.ofInt(progressBar, "progress", 0, 100);
        progressAnimator.setDuration(5000);
        progressAnimator.setInterpolator(new DecelerateInterpolator());
        progressAnimator.setRepeatCount(14);
        progressAnimator.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {
                tvInstruction.setText("Ambil nafas panjang melalui hidung");
                mode++;
            }

            @Override
            public void onAnimationEnd(Animator animator) {
                new Handler().postDelayed(() -> {
                    dialogFeedback();
                }, 3000);
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {
                i++;
                switch (mode){
                    case 1:
                        Picasso.get().load(R.drawable.ic_hold).into(ivIcon);
                        tvInstruction.setText("Tahan nafas");
                        mode++;
                        break;
                    case 2:
                        Picasso.get().load(R.drawable.ic_exhale).into(ivIcon);
                        tvInstruction.setText("Hembuhskan secara perlahan melalui hidung");
                        mode = 0;
                        break;
                    default:
                        Picasso.get().load(R.drawable.ic_inhale).into(ivIcon);
                        tvInstruction.setText("Ambil nafas panjang melalui hidung");
                        mode++;
                        break;
                }
                if (i == 3 || i == 9) {
                    tvInstruction.setText("Ambil nafas secara perlahan, sambil rasakan udara yang masuk melalui kedua lubang hidungmu");
                }else if (i == 4 || i == 10) {
                    tvInstruction.setText("Tahan nafas dan fokuskan pikiranmu hanya pada nafasmu");
                }else if (i == 5 || i == 11){
                    tvInstruction.setText("Hembuskan nafas secara perlahan, sambil rasakan udara yang keluar melalui kedua lubang hidungmu");
                }else if (i == 6 || i == 12){
                    tvInstruction.setText("Letakkan satu tangan diatas perut. Ambil nafas panjang sambil rasakan perutmu yang bergerak.");
                }else if (i == 7 || i == 13){
                    tvInstruction.setText("Tahan nafas dan pusatkan perhatian pada tanganmu yang berada diatas perut");
                }else if (i == 8 || i == 14){
                    tvInstruction.setText("Hembuskan nafas sambil rasakan pergerakan perut dengan tanganmu.");
                }
            }
        });
    }

    @OnClick(R.id.btnAction_deepBreath) void onClick(){
        if (flag){
            flag = false;
            btnAction.setText("Start");
            progressAnimator.pause();
            isPaused = true;
        }else {
            flag = true;
            btnAction.setText("Pause");
            if (isPaused) {
                progressAnimator.resume();
            } else{
                progressAnimator.start();
            }
        }
    }

    @OnClick(R.id.ivButtonClose_deepBreath) void onClose(){
        DialogBuilder.closeDialog(getActivity(), (dialog, which) -> {
            startActivity(new Intent(getContext(), MainActivity.class));
            getActivity().finish();
        });
    }

    private void dialogFeedback(){
        mDialog = new MaterialDialog.Builder(getContext())
                .customView(R.layout.dialog_feedback, true)
                .backgroundColor(getResources().getColor(R.color.primaryLightColor))
                .build();

        TextView tvCoping = (TextView)mDialog.findViewById(R.id.tvCoping_dialogFeedback);
        CardView cvPositive = (CardView)mDialog.findViewById(R.id.cvPositive_dialogFeedback);
        CardView cvNeutral = (CardView)mDialog.findViewById(R.id.cvNeutral_dialogFeedback);
        CardView cvNegative = (CardView)mDialog.findViewById(R.id.cvNegative_dialogFeedback);

        tvCoping.setText("Deep Breath");

        cvPositive.setOnClickListener(view -> {
            startActivity(new Intent(getContext(), MainActivity.class));
            getActivity().finish();
        });

        cvNeutral.setOnClickListener(view -> {
            isPaused = false;
            flag = false;
            btnAction.setText("Start");
            i = 0;
            mDialog.dismiss();
        });

        int selectedCoping = Util.getActiveCoping(getActivity());
        cvNegative.setOnClickListener(view -> {
            mDialog.dismiss();
            // check if last step
            if (selectedCoping == 0){
                startActivity(new Intent(getContext(), MainActivity.class));
                getActivity().finish();
            }else {
                getActivity().getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.fragment_container, Util.getCopingFragment(selectedCoping))
                        .commit();
            }
        });
        mDialog.show();
    }
}
