package com.imaisnaini.bear.ui.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.imaisnaini.bear.R;
import com.imaisnaini.bear.ui.dialog.DialogBuilder;
import com.imaisnaini.bear.ui.util.PrefUtil;
import com.imaisnaini.bear.ui.util.Util;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class CopingActivity extends AppCompatActivity {
    private int selectedCoping = 0;
    private int idCoping = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_coping);
        ButterKnife.bind(this);
        initFragment();
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
    }

    private void initFragment(){
        idCoping = getIntent().getIntExtra("idCoping", 0);
        if (idCoping != 0){
            selectedCoping = idCoping;
        }else {
            selectedCoping = Util.getActiveCoping(getApplication());
        }
        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, Util.getCopingFragment(selectedCoping)).commit();
    }
}
