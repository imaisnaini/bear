package com.imaisnaini.bear.ui.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;

import com.afollestad.materialdialogs.MaterialDialog;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.imaisnaini.bear.R;
import com.imaisnaini.bear.ui.dialog.DialogAnimated;
import com.imaisnaini.bear.ui.dialog.DialogBuilder;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SigninActivity extends AppCompatActivity {
    @BindView(R.id.etEmail)
    TextInputEditText etEmail;
    @BindView(R.id.etPassword)
    TextInputEditText etPassword;
    @BindView(R.id.toolbar)
    Toolbar toolbar;

    private static final String TAG = "SIGN_IN";
    private FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signin);
        ButterKnife.bind(this);
        mAuth = FirebaseAuth.getInstance();
        initViews();
    }

    private void initViews(){
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setTitleTextColor(getResources().getColor(R.color.transparent));

    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @OnClick(R.id.btnSignin) void doSignin(){
        String email = etEmail.getText().toString();
        String pwd = etPassword.getText().toString();

        if (validate()){
            Log.d(TAG, "signin:" + email);

            //final MaterialDialog dialog = DialogBuilder.showLoadingDialog(this, "Updating Data", "Please wait..", true);
            DialogAnimated dialogAnimated = new DialogAnimated("sand_watch.json", true);
            dialogAnimated.show(getSupportFragmentManager(), "Loading");
            mAuth.signInWithEmailAndPassword(email, pwd).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                @Override
                public void onComplete(@NonNull Task<AuthResult> task) {
                    if (task.isSuccessful()){
                        Log.d(TAG, "signInWithEmail:success");
                        FirebaseUser user = mAuth.getCurrentUser();
                        startActivity(new Intent(getApplication(), MainActivity.class));
                        finish();
                    }else {
                        Log.w(TAG, "signInWithEmail:failure", task.getException());
                        DialogBuilder.showErrorDialog(SigninActivity.this, "Gagal Login");
                    }
                    //dialog.dismiss();
                    dialogAnimated.dismiss();
                }
            });
        }
    }

    private boolean validate(){
        boolean valid = true;
        String email = etEmail.getText().toString();
        String pwd = etPassword.getText().toString();

        if (TextUtils.isEmpty(email)){
            etEmail.setError("Reuired!");
            valid = false;
        }
        if (TextUtils.isEmpty(pwd)){
            etPassword.setError("Reuired!");
            valid = false;
        }

        return valid;
    }
}
