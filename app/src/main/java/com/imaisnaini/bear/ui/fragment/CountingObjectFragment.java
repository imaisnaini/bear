package com.imaisnaini.bear.ui.fragment;


import android.app.DialogFragment;
import android.content.Intent;
import android.os.Bundle;

import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;

import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.airbnb.lottie.LottieAnimationView;
import com.imaisnaini.bear.R;
import com.imaisnaini.bear.db.model.Quiz;
import com.imaisnaini.bear.ui.activity.MainActivity;
import com.imaisnaini.bear.ui.dialog.DialogAnimated;
import com.imaisnaini.bear.ui.dialog.DialogBuilder;
import com.imaisnaini.bear.ui.presenter.QuizPresenter;
import com.imaisnaini.bear.ui.util.Util;
import com.imaisnaini.bear.ui.view.QuizView;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * A simple {@link Fragment} subclass.
 */
public class CountingObjectFragment extends Fragment implements QuizView {
    @BindView(R.id.etAnswer_countingObject)
    EditText etAnswer;
    @BindView(R.id.tvQuestion_countingObject)
    TextView tvQuestion;
    @BindView(R.id.ivIcon_countingObject)
    ImageView ivIcon;
    @BindView(R.id.lavLoading_countingObject)
    LottieAnimationView lavLoading;

    private int stage = 0;
    private int limit = 4;
    private QuizPresenter mQuizPresenter;
    private List<Quiz> quizList = new ArrayList<>();
    private MaterialDialog mDialog;
    private DialogAnimated mDialogAnim;

    public CountingObjectFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_counting_object, container, false);
        ButterKnife.bind(this, view);
        initData();
        initViews();
        return view;
    }

    private void initData(){
        mQuizPresenter = new QuizPresenter(this);
        quizList = mQuizPresenter.getRandom(limit);
    }

    private void initViews(){
        lavLoading.setVisibility(View.VISIBLE);
        Picasso.get().load(quizList.get(stage).getIcon()).into(ivIcon, new Callback() {
            @Override
            public void onSuccess() {
                lavLoading.setVisibility(View.GONE);
            }

            @Override
            public void onError(Exception e) {

            }
        });
        tvQuestion.setText(quizList.get(stage).getQuestion());
        etAnswer.setText("");
    }

    private void dialogFeedback(){
        mDialog = new MaterialDialog.Builder(getContext())
                .customView(R.layout.dialog_feedback, true)
                .backgroundColor(getResources().getColor(R.color.primaryLightColor))
                .build();
        TextView tvCoping = (TextView)mDialog.findViewById(R.id.tvCoping_dialogFeedback);
        CardView cvPositive = (CardView)mDialog.findViewById(R.id.cvPositive_dialogFeedback);
        CardView cvNeutral = (CardView)mDialog.findViewById(R.id.cvNeutral_dialogFeedback);
        CardView cvNegative = (CardView)mDialog.findViewById(R.id.cvNegative_dialogFeedback);

        tvCoping.setText("Counting Object");

        cvPositive.setOnClickListener(view -> {
            startActivity(new Intent(getContext(), MainActivity.class));
            getActivity().finish();
        });

        cvNeutral.setOnClickListener(view -> {
            stage = 0;
            quizList = mQuizPresenter.getRandom(limit);
            initViews();
            mDialog.dismiss();
        });

        int selectedCoping = Util.getActiveCoping(getActivity());
        cvNegative.setOnClickListener(view -> {
            mDialog.dismiss();
            // check if last step
            if (selectedCoping == 0){
                startActivity(new Intent(getContext(), MainActivity.class));
                getActivity().finish();
            }else {
                getActivity().getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.fragment_container, Util.getCopingFragment(selectedCoping))
                        .commit();
            }
        });
        mDialog.show();
    }

    @OnClick(R.id.btnSubmit_countingObject) void onSubmit(){
        int answer = Integer.parseInt(etAnswer.getText().toString());
        if (answer == quizList.get(stage).getAnswer()){
            mDialogAnim = new DialogAnimated("check.json", false);
            mDialogAnim.show(getFragmentManager(), "Success");
            new Handler().postDelayed(() -> {
                mDialogAnim.dismiss();
                stage += 1;
                if (stage == limit){
                    dialogFeedback();
                }else {
                    initViews();
                }
            }, 2000);
        }else {
            etAnswer.setError("Belum benar, ayo hitung lagi!");
        }
    }

    @OnClick(R.id.ivButtonClose_countingObject) void onClose(){
        DialogBuilder.closeDialog(getActivity(), (dialog, which) -> {
            startActivity(new Intent(getContext(), MainActivity.class));
            getActivity().finish();
        });
    }

    @Override
    public void showLoading() {

    }

    @Override
    public void hideLoading() {

    }

    @Override
    public void loadQuiz(List<Quiz> list) {

    }
}
