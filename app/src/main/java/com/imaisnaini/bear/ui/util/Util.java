package com.imaisnaini.bear.ui.util;

import android.content.Context;
import android.util.Log;

import androidx.fragment.app.Fragment;

import com.imaisnaini.bear.ui.fragment.CalmBreathingFragment;
import com.imaisnaini.bear.ui.fragment.ColorFragment;
import com.imaisnaini.bear.ui.fragment.CopingCardFragment;
import com.imaisnaini.bear.ui.fragment.CountingObjectFragment;
import com.imaisnaini.bear.ui.fragment.DeepBreathFragment;
import com.imaisnaini.bear.ui.fragment.GroundingFragment;
import com.imaisnaini.bear.ui.fragment.MuscleRelaxationFragment;
import com.imaisnaini.bear.ui.fragment.WorryBoxFragment;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

public class Util {
    public static <T extends Comparable<? super T>> List<T> asSortedList(Collection<T> c) {
        List<T> list = new ArrayList<T>(c);
        java.util.Collections.sort(list, Collections.reverseOrder());
        return list;
    }

    public static Fragment getCopingFragment(int index){
        Fragment fragment = new Fragment();
        if (index == 1){
            fragment = new CalmBreathingFragment();
        }else if(index == 2){
            fragment = new MuscleRelaxationFragment();
        }else if (index == 3){
            fragment = new DeepBreathFragment();
        }else if (index == 4){
            fragment = new ColorFragment();
        }else if (index == 5){
            fragment = new CountingObjectFragment();
        }else if (index == 6){
            fragment = new CopingCardFragment();
        }else if (index == 7){
            fragment = new GroundingFragment();
        }else if (index == 8){
            fragment = new WorryBoxFragment();
        }
        return fragment;
    }

    public static Integer getActiveCoping(Context ctx){
        Set<String> setCoping = PrefUtil.getStringSet(ctx, PrefUtil.COPING_SESSION);
        HashMap<Integer, Float> mapCF = new HashMap<>();
        // seperating id coping from cf value
        for (String s : setCoping){
            Integer key = Integer.valueOf(s.split("-")[0]);
            Float value = Float.valueOf(s.split("-")[1]);
            mapCF.put(key, value);
        }

        Collection collection = mapCF.values();
        List<Float> sortedValues = asSortedList(collection);
        List<Integer> sortedKeys = new ArrayList<>();
        // sorting id coping by cf value
        for (int i = 0; i < sortedValues.size(); i++){
            for (int key : mapCF.keySet()){
                if (sortedValues.get(i).equals(mapCF.get(key))){
                    sortedKeys.add(key);
                }
            }
        }
        // get current step
        Integer step = Integer.valueOf(PrefUtil.getString(ctx, PrefUtil.COPING_STEP));
        Log.i("COPING_STEP", PrefUtil.getString(ctx, PrefUtil.COPING_STEP));
        // check if last step
        if (step == sortedKeys.size()){
            return 0;
        }else {
            // update step
            PrefUtil.putString(ctx, PrefUtil.COPING_STEP, String.valueOf(step + 1));
            return sortedKeys.get(step);
        }
    }
}
