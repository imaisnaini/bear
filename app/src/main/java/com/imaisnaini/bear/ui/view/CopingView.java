package com.imaisnaini.bear.ui.view;

import com.imaisnaini.bear.db.model.Coping;

import java.util.List;

public interface CopingView {
    void showLoading();
    void hideLoading();
    void loadCoping(List<Coping> list);
}
