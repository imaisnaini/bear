package com.imaisnaini.bear.ui.presenter;

import com.imaisnaini.bear.db.dao.QuizDao;
import com.imaisnaini.bear.db.model.Quiz;
import com.imaisnaini.bear.ui.view.QuizView;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class QuizPresenter {
    private QuizView mQuizView;

    public QuizPresenter() {
    }

    public QuizPresenter(QuizView mQuizView) {
        this.mQuizView = mQuizView;
    }

    public List<Quiz> getAll(){
        List<Quiz> list = new ArrayList<>();
        try {
            list = QuizDao.getInstance().read();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return list;
    }

    public List<Quiz> getRandom(int limit){
        List<Quiz> listAll = getAll();
        List<Quiz> listRandom = new ArrayList<>();
        List<Integer> listID = new ArrayList<>();
        int min = 1;
        int max = listAll.size();

        while (listRandom.size() <= limit){
            int rand = (int) ((Math.random() * (max - min)) + min);
            if (!listID.contains(rand)){
                listID.add(rand);
                listRandom.add(listAll.get(rand));
            }
        }

        return listRandom;
    }

    public void initiateData(){
        List<Quiz> list = new ArrayList<>();
        list.add(new Quiz(1, "Ada berapa buku dalam gambar ?", 11, "https://firebasestorage.googleapis.com/v0/b/ta-bear.appspot.com/o/books.jpg?alt=media&token=fc38b04d-31be-4d2b-9a90-c06926593875"));
        list.add(new Quiz(2, "Ada berapa potongan buah Tin dalam gambar ?", 20, "https://firebasestorage.googleapis.com/v0/b/ta-bear.appspot.com/o/tin.jpg?alt=media&token=430f1fa9-ffd7-4577-84f1-1bfbe3378145"));
        list.add(new Quiz(3, "Ada berapa kelopak bunga dalam gambar ?", 17, "https://firebasestorage.googleapis.com/v0/b/ta-bear.appspot.com/o/rose_petals.jpg?alt=media&token=5a10f269-aa4c-450f-90ce-44c983a5361c"));
        list.add(new Quiz(4, "Ada berapa anak anjing dalam gambar ?", 5, "https://firebasestorage.googleapis.com/v0/b/ta-bear.appspot.com/o/puppies.jpg?alt=media&token=83c2567c-1a6c-49a3-b65e-555e0d32b430"));
        list.add(new Quiz(5, "Hitung jumlah spidol dalam gambar!", 16, "https://firebasestorage.googleapis.com/v0/b/ta-bear.appspot.com/o/marker.jpg?alt=media&token=9f709c1f-7b4d-40ef-8c73-182aa5e63843"));
        list.add(new Quiz(6, "Hitung jumlah bawang putih dalam gambar!", 15, "https://firebasestorage.googleapis.com/v0/b/ta-bear.appspot.com/o/garlic.jpg?alt=media&token=1e18bd06-4580-41a1-a10a-8fe6247510d1"));
        list.add(new Quiz(7, "Ada berapa macarons dalam piring ?", 11, "https://firebasestorage.googleapis.com/v0/b/ta-bear.appspot.com/o/macarons.jpg?alt=media&token=fc6191d3-eafa-4a4f-ab69-8e211ceb953b"));
        list.add(new Quiz(8, "Ada berapa jenis buah dalam gambar ?", 8, "https://firebasestorage.googleapis.com/v0/b/ta-bear.appspot.com/o/fruits.jpg?alt=media&token=e87fb9ff-0e6a-415e-abe7-a744ea11b72c"));
        list.add(new Quiz(9, "Ada berapa putik bunga yang lepas ?", 24, "https://firebasestorage.googleapis.com/v0/b/ta-bear.appspot.com/o/daisy.jpg?alt=media&token=c8e0e228-12e7-43a3-8611-54cb46ddd78a"));
        list.add(new Quiz(10, "Ada berapa orang dalam pohon keluarga ?", 16, "https://firebasestorage.googleapis.com/v0/b/ta-bear.appspot.com/o/family_tree.jpg?alt=media&token=7ecd94f1-e60e-446b-be26-ba650a6807ae"));
        list.add(new Quiz(11, "Hitung jumlah biji kopi dalam gambar", 17, "https://firebasestorage.googleapis.com/v0/b/ta-bear.appspot.com/o/coffee_bean.jpg?alt=media&token=f064b759-7a5e-4bf5-87e4-66684c91e473"));
        list.add(new Quiz(12, "Ada berapa clip berwarna biru dalam gambar ?", 10, "https://firebasestorage.googleapis.com/v0/b/ta-bear.appspot.com/o/paper_clip.jpg?alt=media&token=7b74dc06-2c36-424c-9db8-1ccfce65039d"));
        list.add(new Quiz(13, "Hitung jumlah sticky note dalam gambar", 18, "https://firebasestorage.googleapis.com/v0/b/ta-bear.appspot.com/o/sticky_notes.jpg?alt=media&token=f49a9cb8-13ed-4b83-98e5-2984a48a8e22"));
        list.add(new Quiz(14, "Ada berapa potongan puzel yang hilang dalam gambar ?", 18, "https://firebasestorage.googleapis.com/v0/b/ta-bear.appspot.com/o/puzzle.jpg?alt=media&token=cfca2c71-9323-4e02-96dc-df627696d26d"));
        list.add(new Quiz(15, "Hitung jumlah botol pada rak", 21, "https://firebasestorage.googleapis.com/v0/b/ta-bear.appspot.com/o/bar.jpg?alt=media&token=7d3ecc8a-3490-456c-b492-91ca2f8e841c"));
        list.add(new Quiz(16, "Ada berapa buku dalam gambar ?", 12, "https://firebasestorage.googleapis.com/v0/b/ta-bear.appspot.com/o/book.jpg?alt=media&token=ed2c6d31-1847-46f3-8aa7-20e8e0db6440"));
        list.add(new Quiz(16, "Ada berapa lilin dalam gambar ?", 9, "https://firebasestorage.googleapis.com/v0/b/ta-bear.appspot.com/o/candle.jpg?alt=media&token=dd6d5ae9-8460-49ba-b36f-85390d8e454e"));

        for (Quiz quiz : list) {
            try {
                QuizDao.getInstance().add(quiz);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
}
