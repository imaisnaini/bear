package com.imaisnaini.bear.ui.presenter;

import com.imaisnaini.bear.db.dao.QuoteDao;
import com.imaisnaini.bear.db.model.Quote;
import com.imaisnaini.bear.ui.view.QuoteView;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class QuotePresenter {
    private QuoteView mQuoteView;

    public QuotePresenter() {
    }

    public QuotePresenter(QuoteView mQuoteView) {
        this.mQuoteView = mQuoteView;
    }

    public List<Quote> getByCard(int idCard){
        List<Quote> list = new ArrayList<>();
        try {
            list = QuoteDao.getInstance().getByCard(idCard);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return list;
    }

    public void initiateData(){
        mQuoteView.showLoading();

        List<String> itemPanic = new ArrayList<>();
        itemPanic.add("Ini tidak berbahaya.");
        itemPanic.add("Aku akan membiarkan tubuhku melewati ini.");
        itemPanic.add("Aku pernah berhasil melewati serangan panik dimasa lalu dan aku juga akan berhasil kali ini.");
        itemPanic.add("Tidak akan ada hal serius yang akan terjadi.");
        itemPanic.add("Ini akan segera berlalu.");
        itemPanic.add("Aku bisa mengendalikan rasa sakit ini.");
        itemPanic.add("Ini tidak akan bertahan dalam waktu yang lama.");
        itemPanic.add("Ini tidak seburuk yang aku pikirkan.");

        List<String> itemAnxiety = new ArrayList<>();
        itemAnxiety.add("Perasaan ini tidak nyaman, tapi aku bisa mengatasinya.");
        itemAnxiety.add("Dengan bersantai melalui perasaan ini aku belajar untuk mengjadapi rasa takutku.");
        itemAnxiety.add("Aku boleh merasa gelisah dan tetap bisa mengatasi situasi ini.");
        itemAnxiety.add("Perasaan ini akan pergi.");
        itemAnxiety.add("Semua ini hanya pikiranku, bukan sebuah kenyataan.");
        itemAnxiety.add("Kegelisahaan ini tidak akan menyakitiku.");
        itemAnxiety.add("Aku sudah pernah melewati ini sebelumnya jadi aku bisa melewatinya lagi.");
        itemAnxiety.add("Aku akan melakukan yang terbaik yang aku bisa.");
        itemAnxiety.add("Dengan menghadapi rasa takutku, aku bisa mengalahkannya.");
        itemAnxiety.add("Apapun yang terjadi, terjadilah. Aku bisa menghadapinya.");

        List<String> itemReminder = new ArrayList<>();
        itemReminder.add("Mengkritik diri sendiri tidak akan membantu, itu hanya akan menambah masalah dengan membuatku merasa semakin buruk.");
        itemReminder.add("Coba perkecil masalah ini dan lihat gambar keseluruhannya.");
        itemReminder.add("Aku tidak akan membiarkan masa laluku mendefinisikan ku.");
        itemReminder.add("Sebuah progress tidak selalu berjalan mulus. Sabar dan jangan menyerah.");
        itemReminder.add("Menjadi sempurna itu tidak mungkin.");
        itemReminder.add("Hari ini mungkin berat, tapi tidak akan menghapus progress yang telah ku buat.");
        itemReminder.add("Toleransi ketidakpastian.");
        itemReminder.add("Berhenti berfokus pada masa lalu.");
        itemReminder.add("Membuat kesalahan bukan berarti aku gagal. Semua orang pernah membuat kesalahan.");
        itemReminder.add("Aku layak untuk bahagia.");

        List<String> itemIslamic = new ArrayList<>();
        itemIslamic.add("Hai orang-orang yang beriman, jadikanlah sabar dan shalat sebagai penolongmu, sesungguhnya Allah bersama orang-orang yang sabar.");
        itemIslamic.add("Sesungguhnya hanyalah kepada Allah aku mengadukan kesusahan dan kesedihanku, dan aku mengetahui dari Allah apa yang kamu tiada mengetahuinya.");
        itemIslamic.add("Sesungguhnya pada hari ini Aku memberi balasan kepada mereka, karena kesabaran mereka \n Sesungguhnya mereka itulah orang-orang yang memperoleh kemenangan.");
        itemIslamic.add("Tidaklah menimpa seorang mukmin rasa sakit yang terus menerus, kepayahan, penyakit, dan juga kesedihan, bahkan sampai kesusahan yang menyusahkannya, melainkan akan dihapuskan dengannya dosa-dosanya.");
        itemIslamic.add("Barangsiapa membiasakan diri untuk beristighfar, Allah akan memberikan jalan keluar baginya dari setiap kesulitan dan kelegaan dari setiap kecemasan, dan akan memberi rezeki dari arah yang tidak disangka-sangka.");
        itemIslamic.add("(yaitu) Orang-orang yang beriman dan hati mereka menjadi tenteram dengan mengingat Allah. Ingatlah, hanya dengan mengingat Allah hati menjadi tenteram.");
        itemIslamic.add("Janganlah kamu bersikap lemah, dan janganlah (pula) kamu bersedih hati, padahal kamulah orang-orang yang paling tinggi (derajatnya), jika kamu orang-orang yang beriman.");
        List<String> authorIslamic = new ArrayList<>();
        authorIslamic.add("QS Al-Baqarah : 153");
        authorIslamic.add("QS Yusuf : 86");
        authorIslamic.add("QS Al-Mu'minun : 111");
        authorIslamic.add("HR Muslim");
        authorIslamic.add("HR Abu Dawud");
        authorIslamic.add("QS Ar-Rad : 28");
        authorIslamic.add("QS Ali-Imran : 139");

        List<String> itemMuslim = new ArrayList<>();
        itemMuslim.add("Memangnya kenapa kalau hidup ini tidak sempurna, toh ini bukan Jannah (surga).");
        itemMuslim.add("Saat kita memperbaiki hubungan kita dengan Allah \n Allah akan memperbaiki segala sesuatu yang lain untuk kita.");
        itemMuslim.add("Dia yang senantiasa menjaga hatinya dekat dengan Tuhan akan menemukan kedamaian dan ketentraman, sementara dia yang senantiasa memberikan hatinya kepada manusia akan menemukan kegelisahan dan ketakutan.");
        itemMuslim.add("Tidak ada sesuatu yang paling disukai oleh setan, melainkan melihat seorang mukmin yang berhati murung.");
        itemMuslim.add("Jangan biarkan kesulitanmu mengisimu dengan kecemasan, karena hanya di malam-malam paling gelap bintang-bintang bersinar lebih cemerlang.");
        List<String> authorMuslim = new ArrayList<>();
        authorMuslim.add("Nourman Ali Khan");
        authorMuslim.add("Dr. Bilal Philips");
        authorMuslim.add("Ibnu Qayyim");
        authorMuslim.add("Ibnu Qayyim");
        authorMuslim.add("Ali bin Abi Thalib");

        int i;
        int j = 0;
        for (i = 0; i < itemAnxiety.size(); i++){
            j = j+1;
            try {
                QuoteDao.getInstance().add(new Quote(j, 1, itemAnxiety.get(i)));
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        for (i = 0; i < itemPanic.size(); i++){
            j = j+1;
            try {
                QuoteDao.getInstance().add(new Quote(j, 2, itemPanic.get(i)));
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        for (i = 0; i < itemPanic.size(); i++){
            j = j+1;
            try {
                QuoteDao.getInstance().add(new Quote(j, 3, itemReminder.get(i)));
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        for (i = 0; i < itemIslamic.size(); i++){
            j = j+1;
            try {
                QuoteDao.getInstance().add(new Quote(j, 4, itemIslamic.get(i), authorIslamic.get(i)));
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        for (i = 0; i < itemMuslim.size(); i++){
            j = j+1;
            try {
                QuoteDao.getInstance().add(new Quote(j, 5, itemMuslim.get(i), authorMuslim.get(i)));
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        mQuoteView.hideLoading();
    }
}
