package com.imaisnaini.bear.ui.view;

import com.imaisnaini.bear.db.model.Symptom;

import java.util.List;

public interface SymptomView {
    void showLoading();
    void hideLoading();
    void loadSymptom(List<Symptom> list);
}
