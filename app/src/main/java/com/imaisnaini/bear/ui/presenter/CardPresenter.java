package com.imaisnaini.bear.ui.presenter;

import com.imaisnaini.bear.db.dao.CardDao;
import com.imaisnaini.bear.db.model.Card;
import com.imaisnaini.bear.ui.view.CCardView;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class CardPresenter {
    private CCardView mCCardView;

    public CardPresenter() {
    }

    public CardPresenter(CCardView mCCardView) {
        this.mCCardView = mCCardView;
    }

    public List<Card> getList(){
        List<Card> list = new ArrayList<>();
        try {
            list = CardDao.getInstance().read();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return list;
    }

    public void initiateData(){
        mCCardView.showLoading();

        Card cardAnxiety = new Card(1, "Anxiety Card", "Bear");
        Card cardPanic = new Card(2, "Panic Card", "Bear");
        Card cardReminder = new Card(3, "Reminder Card", "Bear");
        Card cardIslamic = new Card(4, "Islamic Card", "Al-Quran & Hadist");
        Card cardTokohMuslim = new Card(5, "Islamic Card", "Tokoh Muslim");

        try {
            CardDao.getInstance().add(cardAnxiety);
            CardDao.getInstance().add(cardPanic);
            CardDao.getInstance().add(cardReminder);
            CardDao.getInstance().add(cardIslamic);
            CardDao.getInstance().add(cardTokohMuslim);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        mCCardView.hideLoading();
    }
}
