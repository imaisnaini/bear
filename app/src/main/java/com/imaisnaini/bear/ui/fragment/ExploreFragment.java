package com.imaisnaini.bear.ui.fragment;


import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.imaisnaini.bear.R;
import com.imaisnaini.bear.ui.activity.CopingActivity;
import com.imaisnaini.bear.ui.util.PrefUtil;

import java.util.HashSet;

import butterknife.ButterKnife;
import butterknife.OnClick;


/**
 * A simple {@link Fragment} subclass.
 */
public class ExploreFragment extends Fragment {
    private HashSet<String> copingSet = new HashSet<>();

    public ExploreFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_explore, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    private void putSession(){
        PrefUtil.putStringSet(getActivity(), PrefUtil.COPING_SESSION, copingSet);
        PrefUtil.putString(getActivity(), PrefUtil.COPING_STEP, String.valueOf(0));
        startActivity(new Intent(getActivity(), CopingActivity.class));
    }

    @OnClick(R.id.layoutDeepBreath_explore) void goDeepBreath(){
        copingSet.clear();
        copingSet.add("3-1");
        putSession();
    }

    @OnClick(R.id.layoutCalmBreathing_explore) void goCalmBreathing(){
        copingSet.clear();
        copingSet.add("1-1");
        putSession();
    }

    @OnClick(R.id.layoutColors_explore) void goColors(){
        copingSet.clear();
        copingSet.add("4-1");
        putSession();
    }

    @OnClick(R.id.layoutWorryBox_explore) void goWorryBox(){
        copingSet.clear();
        copingSet.add("8-1");
        putSession();
    }

    @OnClick(R.id.layoutCopingCards_explore) void goCopingCards(){
        copingSet.clear();
        copingSet.add("6-1");
        putSession();
    }

    @OnClick(R.id.layoutGrounding_explore) void goGrounding(){
        copingSet.clear();
        copingSet.add("7-1");
        putSession();
    }

    @OnClick(R.id.layoutCountingObject_explore) void goFocused(){
        copingSet.clear();
        copingSet.add("5-1");
        putSession();
    }

    @OnClick(R.id.layoutMuscleRelax_explore) void goMuscleRelax(){
        copingSet.clear();
        copingSet.add("2-1");
        putSession();
    }

}
