package com.imaisnaini.bear.ui.fragment;


import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.imaisnaini.bear.R;
import com.imaisnaini.bear.ui.adapter.CaseAdapter;
import com.imaisnaini.bear.db.model.Case;
import com.imaisnaini.bear.ui.presenter.CasePresenter;
import com.imaisnaini.bear.ui.view.CaseView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends Fragment implements CaseView {
    @BindView(R.id.home_tvUsername)
    TextView tvUsername;
    @BindView(R.id.home_rvContent)
    RecyclerView rvContent;

    private static final String TAG = "HOME";
    private CaseAdapter mAdapter;
    private List<Case> mCaseList =  new ArrayList<>();
    private Case mCase;
    private CasePresenter mCasePresenter;
    private FirebaseUser mUser;
    private FirebaseAuth mAuth;

    public HomeFragment() {
        // Required empty public constructor
    }

    @Override
    public void onResume() {
        super.onResume();
        mCasePresenter.loadData();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        ButterKnife.bind(this, view);
        init();
        return view;
    }

    private void init(){
        mCasePresenter = new CasePresenter(this);

        mAuth = FirebaseAuth.getInstance();
        mUser = mAuth.getCurrentUser();
        tvUsername.setText(mUser.getDisplayName());

        rvContent.setHasFixedSize(true);
        rvContent.setLayoutManager(new LinearLayoutManager(getContext()));

        mAdapter = new CaseAdapter(getContext());
    }

    @Override
    public void showLoading() {

    }

    @Override
    public void hideLoading() {

    }

    @Override
    public void loadCase(List<Case> list) {
        mAdapter.generate(list);
        rvContent.setAdapter(mAdapter);
        mAdapter.notifyDataSetChanged();
    }
}
