package com.imaisnaini.bear.ui.presenter;

import android.content.Context;

import com.imaisnaini.bear.db.dao.HistoryDao;
import com.imaisnaini.bear.db.model.History;
import com.imaisnaini.bear.ui.view.HistoryView;

import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;

public class HistoryPresenter {
    private HistoryView mHistoryView;

    public HistoryPresenter(HistoryView mHistoryView) {
        this.mHistoryView = mHistoryView;
    }

    public void add(int idCase, HashMap<Integer, Float> cfUser){
        Calendar c = Calendar.getInstance();
        SimpleDateFormat sdfDate = new SimpleDateFormat("dd-MM-yyyy");
        String strDate = sdfDate.format(c.getTime()).replace("-", "");
        Long tgl = Long.parseLong(strDate);

        try {
            HistoryDao.getInstance().add(new History(idCase, cfUser, tgl));
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
