package com.imaisnaini.bear.ui.presenter;

import com.imaisnaini.bear.db.dao.CFPakarDao;
import com.imaisnaini.bear.db.dao.SymptomDao;
import com.imaisnaini.bear.db.model.Symptom;
import com.imaisnaini.bear.ui.view.SymptomView;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class SymptomPresenter {
    private SymptomView mSymptomView;

    public SymptomPresenter() {
    }

    public SymptomPresenter(SymptomView mSymptomView) {
        this.mSymptomView = mSymptomView;
    }

    public Symptom getByID(int id){
        Symptom symptom = new Symptom();
        try {
            symptom = SymptomDao.getInstance().getByID(id);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return symptom;
    }

    public void loadByCase(int idCase){
        mSymptomView.showLoading();
        List<Integer> listID = new ArrayList<>();
        List<Symptom> symptomList = new ArrayList<>();

        try {
            listID = CFPakarDao.getInstance().getSymptoms(idCase);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        for (int i = 0; i < listID.size(); i++){
            try {
                Symptom symptom = SymptomDao.getInstance().getByID(listID.get(i));
                symptomList.add(symptom);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        mSymptomView.loadSymptom(symptomList);
        mSymptomView.hideLoading();
    }

    public void initiateData(){
        mSymptomView.showLoading();
        List<Symptom> list = new ArrayList<>();

        list.add(new Symptom(1, "Berkeringat"));
        list.add(new Symptom(2, "Nyeri dada"));
        list.add(new Symptom(3, "Nafas pendek / cepat"));
        list.add(new Symptom(4, "Pusing"));
        list.add(new Symptom(5, "Mual"));
        list.add(new Symptom(6, "Nyeri / kram perut"));
        list.add(new Symptom(7, "Jantung berdebar"));
        list.add(new Symptom(8, "Tangan / kaki mati rasa"));
        list.add(new Symptom(9, "Tangan gemetar"));
        list.add(new Symptom(10, "Kaki kesemutan"));
        list.add(new Symptom(11, "Merasa tersedak"));
        list.add(new Symptom(12, "Otot tegang"));
        list.add(new Symptom(13, "Mulut kering"));
        list.add(new Symptom(14, "Tubuh terasa dingin / panas"));
        list.add(new Symptom(15, "Merasa sekarat / akan mati"));
        list.add(new Symptom(16, "Merasa terancam"));
        list.add(new Symptom(17, "Takut kehilangan kendali"));
        list.add(new Symptom(18, "Merasa akan pingsan"));
        list.add(new Symptom(19, "Kewalahan"));
        list.add(new Symptom(20, "Tidak berdaya"));
        list.add(new Symptom(21, "Cemas"));
        list.add(new Symptom(22, "Bingung"));
        list.add(new Symptom(23, "Khawatir"));
        list.add(new Symptom(24, "Sulit berkonsentrasi"));
        list.add(new Symptom(25, "Mudah tersinggung"));
        list.add(new Symptom(26, "Overthinking"));
        list.add(new Symptom(27, "Stress"));
        list.add(new Symptom(28, "Merasa diri rendah"));
        list.add(new Symptom(29, "Takut"));
        list.add(new Symptom(30, "Putus asa"));
        list.add(new Symptom(31, "Sulit / gangguan tidur"));
        list.add(new Symptom(32, "Lelah"));
        list.add(new Symptom(33, "Tangan berkeringat"));
        list.add(new Symptom(34, "Penglihatan membayang"));

        for (Symptom symptom : list){
            try {
                SymptomDao.getInstance().add(symptom);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        mSymptomView.hideLoading();
    }

}
