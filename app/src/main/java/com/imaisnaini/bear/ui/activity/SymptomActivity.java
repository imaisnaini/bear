package com.imaisnaini.bear.ui.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.imaisnaini.bear.R;
import com.imaisnaini.bear.db.model.Case;
import com.imaisnaini.bear.db.model.Symptom;
import com.imaisnaini.bear.ui.adapter.SymptomAdapter;
import com.imaisnaini.bear.ui.presenter.CasePresenter;
import com.imaisnaini.bear.ui.presenter.SymptomPresenter;
import com.imaisnaini.bear.ui.view.CaseView;
import com.imaisnaini.bear.ui.view.SymptomView;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SymptomActivity extends AppCompatActivity implements SymptomView, CaseView {
    @BindView(R.id.symptom_rvSymptom)
    RecyclerView rvSymptom;
    @BindView(R.id.symptom_ivIconCase)
    ImageView ivIcon;
    @BindView(R.id.symptom_tvCase)
    TextView tvCase;
    @BindView(R.id.toolbar)
    Toolbar toolbar;

    private SymptomAdapter mAdapter;
    private SymptomPresenter mSymptomPresenter;
    private CasePresenter mCasePresenter;
    private List<Symptom> mList = new ArrayList<>();
    private Case mCase;
    private int idCase;

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
        mSymptomPresenter.loadByCase(idCase);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_symptom);
        ButterKnife.bind(this);
        initData();
        initViews();
    }

    private void initData(){
        idCase = getIntent().getIntExtra("idCase", 0);
        mSymptomPresenter = new SymptomPresenter(this);
        mCasePresenter = new CasePresenter(this);
        mCase = mCasePresenter.getByID(idCase);

    }

    private void initViews(){
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setTitleTextColor(getResources().getColor(R.color.transparent));

        rvSymptom.setHasFixedSize(true);
        rvSymptom.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        mAdapter = new SymptomAdapter(getApplicationContext());

        Picasso.get().load(mCase.getIcon()).into(ivIcon);
        tvCase.setText(mCase.getName());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.symptom_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case R.id.menu_next:
                // Converting hashmap cfValue ke array untuk di parsing
                Integer[] listID = new Integer[mAdapter.listCFUser().size()];
                Float[] listCF = new Float[mAdapter.listCFUser().size()];
                mAdapter.listCFUser().keySet().toArray(listID);
                mAdapter.listCFUser().values().toArray(listCF);
                float[] listCFF = new float[listCF.length];
                int[] listIDI = new int[listID.length];
                // parsing to primitive data type
                for(int i = 0; i < listCF.length; i++){
                    listCFF[i] = listCF[i];
                    listIDI[i] = listID[i];
                }

                Bundle bundle = new Bundle();
                bundle.putInt("idCase", idCase);
                bundle.putIntArray("listID", listIDI);
                bundle.putFloatArray("listCF", listCFF);
                Intent intent = new Intent(SymptomActivity.this, ResultActivity.class);
                intent.putExtras(bundle);
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);

        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void showLoading() {

    }

    @Override
    public void hideLoading() {

    }

    @Override
    public void loadCase(List<Case> list) {

    }

    @Override
    public void loadSymptom(List<Symptom> list) {
        mAdapter.generate(list);
        rvSymptom.setAdapter(mAdapter);
        mAdapter.notifyDataSetChanged();
    }
}
