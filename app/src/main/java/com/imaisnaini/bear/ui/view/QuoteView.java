package com.imaisnaini.bear.ui.view;

import com.imaisnaini.bear.db.model.Quote;

import java.util.List;

public interface QuoteView {
    void showLoading();
    void hideLoading();
    void loadQuote(List<Quote> list);
}
