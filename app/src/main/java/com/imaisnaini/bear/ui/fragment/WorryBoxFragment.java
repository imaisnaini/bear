package com.imaisnaini.bear.ui.fragment;


import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.imaisnaini.bear.R;
import com.imaisnaini.bear.ui.activity.MainActivity;
import com.imaisnaini.bear.ui.adapter.GeneralAdapter;
import com.imaisnaini.bear.ui.dialog.DialogBuilder;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * A simple {@link Fragment} subclass.
 */
public class WorryBoxFragment extends Fragment {
    @BindView(R.id.rvContent_worryBox)
    RecyclerView rvContent;
    @BindView(R.id.tvIsEmptyu_worryBox)
    TextView tvIsEmpty;

    private List<String> listWorry = new ArrayList<>();
    private GeneralAdapter mAdapter;

    public WorryBoxFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_worry_box, container, false);
        ButterKnife.bind(this, view);
        init();
        return view;
    }

    private void init(){
        mAdapter = new GeneralAdapter(getContext(), listWorry);
        rvContent.setLayoutManager(new LinearLayoutManager(getContext()));
        rvContent.setAdapter(mAdapter);
    }

    @OnClick(R.id.btnAdd_worryBox) void onAdd(){
        DialogBuilder.showInputDialog(getActivity(), R.string.dialog_worry_title, R.string.dialog_worry_content, (dialog, input) -> {
            listWorry.add(input.toString());
            mAdapter.notifyDataSetChanged();
            tvIsEmpty.setVisibility(View.GONE);
            dialog.dismiss();
        });
    }

    @OnClick(R.id.btnClear_worryBox) void onClear(){
        listWorry.clear();
        mAdapter.notifyDataSetChanged();
        tvIsEmpty.setVisibility(View.VISIBLE);
    }

    @OnClick(R.id.ivButtonClose_worryBox) void onClose(){
        DialogBuilder.closeDialog(getActivity(), (dialog, which) -> {
            startActivity(new Intent(getContext(), MainActivity.class));
            getActivity().finish();
        });
    }
}
