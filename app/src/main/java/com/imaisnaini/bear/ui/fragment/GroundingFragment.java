package com.imaisnaini.bear.ui.fragment;


import android.content.Intent;
import android.os.Build;
import android.os.Bundle;

import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.airbnb.lottie.LottieAnimationView;
import com.imaisnaini.bear.R;
import com.imaisnaini.bear.ui.activity.MainActivity;
import com.imaisnaini.bear.ui.adapter.GeneralAdapter;
import com.imaisnaini.bear.ui.dialog.DialogBuilder;
import com.imaisnaini.bear.ui.util.Util;
import com.shuhart.stepview.StepView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * A simple {@link Fragment} subclass.
 */
public class GroundingFragment extends Fragment {
    @BindView(R.id.stepView_grounding)
    StepView stepView;
    @BindView(R.id.tvInstruction_grounding)
    TextView tvInstruction;
    @BindView(R.id.tvStepInstruction_grounding)
    TextView tvStep;
    @BindView(R.id.rvContent_grounding)
    RecyclerView rvContent;
    @BindView(R.id.fabNext_grounding)
    Button btnNext;
    @BindView(R.id.lavGrounding)
    LottieAnimationView lavAnimation;

    private MaterialDialog mDialog;
    private List<String> listInput = new ArrayList<>();
    private HashMap<Integer, List<String>> finalList = new HashMap<>();
    private boolean isNext = false;
    private GeneralAdapter mAdapter;
    private String titleDialog = "";
    private int[] minimInput = new int[]{5, 4, 3, 2, 1};
    private String[] listLAV = new String[]{"bear_with_glassess.json", "hand.json", "headphone.json", "moncong_bear.json", "rainbow_heart.json"};
    private String[] listQuestion = new String[]{"5 benda yang kamu lihat beserta warnanya",
                                                "4 benda yang bisa kamu sentuh beserta teksturnya",
                                                "3 suara yang kamu dengar",
                                                "2 aroma yang kamu cium",
                                                "1 emosi yang kamu rasakan"};

    public GroundingFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_grounding, container, false);
        ButterKnife.bind(this, view);
        initView();
        reInitiate(0);
        return view;
    }

    private void initView(){
        tvInstruction.setText("Selama terapi ini berjalan, ambillah nafas secara perlahan. " +
                "Tarik nafas melalui hidung, dan keluarkan melalui mulut. Perhatikan sekelilingmu " +
                "dengan seksama tanpa perlu menghakimi.");
        stepView.setOnStepClickListener(step -> {
            stepViewListener(stepView.getCurrentStep(), step);
        });

        mAdapter = new GeneralAdapter(getActivity());
        rvContent.setHasFixedSize(true);
        rvContent.setLayoutManager(new LinearLayoutManager(getActivity()));
    }

    private void reInitiate(int step){
        //int step = stepView.getCurrentStep();
        tvStep.setText("Sebutkan sedikitnya "+ listQuestion[step]);
        lavAnimation.setAnimation(listLAV[step]);
        lavAnimation.playAnimation();
        lavAnimation.loop(true);

        if (finalList.containsKey(step)){
            listInput.clear();
            for (String value : finalList.get(step)){
                listInput.add(value);
            }
            mAdapter.generate(listInput);
        }

        if (listInput.size() >= minimInput[step]){
            btnNext.setEnabled(true);
        }else {
            btnNext.setEnabled(false);
        }
        if (step == 4){
            btnNext.setText("Finish");
        }else {
            btnNext.setText("Next");
        }
    }

    private void stepViewListener(int step, int goStep){
        if (listInput.size() >= minimInput[step]) {
            if (finalList.containsKey(step)){
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    finalList.replace(step, listInput);
                }else {
                    finalList.remove(step);
                    finalList.put(step, listInput);
                }
            }else {
                finalList.put(step, listInput);
            }
            listInput.clear();
            mAdapter.notifyDataSetChanged();
            stepView.go(goStep, true);
            reInitiate(goStep);
        }else {
            showWarnDialog();
        }
    }

    private boolean validateList(int step){
        if (step == 0 && listInput.size() >= 5)
            return true;
        else if (step == 1 && listInput.size() >= 4)
            return true;
        else if (step == 2 && listInput.size() >= 3)
            return true;
        else if (step == 3 && listInput.size() >= 2)
            return true;
        else return step == 4 && listInput.size() >= 1;
    }

    @Override
    public void onResume() {
        super.onResume();
        rvContent.setAdapter(mAdapter);
        mAdapter.generate(listInput);
    }

    @OnClick(R.id.fabNext_grounding) void onNext(){
        if (stepView.getCurrentStep() < 4) {
            stepViewListener(stepView.getCurrentStep(), stepView.getCurrentStep() + 1);
        }else {
            dialogFeedback();
        }
    }

    @OnClick(R.id.fabTulis_grounding) void onWrite(){
        if (stepView.getCurrentStep() == 0){
            titleDialog = "Apa yang kamu lihat ?";
        }else if (stepView.getCurrentStep() == 1){
            titleDialog = "Apa yang kamu sentuh ?";
        }else if (stepView.getCurrentStep() == 2){
            titleDialog = "Apa yang kamu dengar ?";
        }else if (stepView.getCurrentStep() == 3){
            titleDialog = "Apa yang kamu cium ?";
        }else {
            titleDialog = "Apa yang kamu rasakan ?";
        }
        DialogBuilder.showInputDialog(getActivity(), titleDialog, R.string.dialog_grounding_content, (dialog, input) -> {
            listInput.add(input.toString());
            mAdapter.notifyDataSetChanged();
            if (validateList(stepView.getCurrentStep())){
                btnNext.setEnabled(true);
            }else {
                btnNext.setEnabled(false);
            }
            dialog.dismiss();
        });
    }

    @OnClick(R.id.ivButtonClose_grounding) void onClose(){
        DialogBuilder.closeDialog(getActivity(), (dialog, which) -> {
            startActivity(new Intent(getContext(), MainActivity.class));
            getActivity().finish();
        });
    }

    private void showWarnDialog(){
        DialogBuilder.alertDialog(getActivity(), "Grounding", "List kamu belum lengkap nih, ayo isi lagi!");
    }

    private void dialogFeedback(){
        mDialog = new MaterialDialog.Builder(getContext())
                .customView(R.layout.dialog_feedback, true)
                .backgroundColor(getResources().getColor(R.color.primaryLightColor))
                .build();

        TextView tvCoping = (TextView)mDialog.findViewById(R.id.tvCoping_dialogFeedback);
        CardView cvPositive = (CardView)mDialog.findViewById(R.id.cvPositive_dialogFeedback);
        CardView cvNeutral = (CardView)mDialog.findViewById(R.id.cvNeutral_dialogFeedback);
        CardView cvNegative = (CardView)mDialog.findViewById(R.id.cvNegative_dialogFeedback);

        tvCoping.setText("Grounding");

        cvPositive.setOnClickListener(view -> {
            startActivity(new Intent(getContext(), MainActivity.class));
            getActivity().finish();
        });

        cvNeutral.setOnClickListener(view -> {
            mDialog.dismiss();
        });

        int selectedCoping = Util.getActiveCoping(getActivity());
        cvNegative.setOnClickListener(view -> {
            mDialog.dismiss();
            // check if last step
            if (selectedCoping == 0){
                startActivity(new Intent(getContext(), MainActivity.class));
                getActivity().finish();
            }else {
                getActivity().getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.fragment_container, Util.getCopingFragment(selectedCoping))
                        .commit();
            }
        });
        mDialog.show();
    }
}
