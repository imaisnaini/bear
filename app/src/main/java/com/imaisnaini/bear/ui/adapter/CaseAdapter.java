package com.imaisnaini.bear.ui.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.style.TtsSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.imaisnaini.bear.R;
import com.imaisnaini.bear.ui.activity.CopingActivity;
import com.imaisnaini.bear.ui.activity.SymptomActivity;
import com.imaisnaini.bear.db.model.Case;
import com.imaisnaini.bear.ui.dialog.DialogBuilder;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class CaseAdapter extends RecyclerView.Adapter<CaseAdapter.ViewHolder> {
    private List<Case> mList = new ArrayList<>();
    Context ctx;

    public CaseAdapter(Context ctx) {
        this.ctx = ctx;
    }

    public CaseAdapter(List<Case> mList, Context ctx) {
        this.mList = mList;
        this.ctx = ctx;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_case, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.idCase = mList.get(position).getId();
        holder.tvTitle.setText(mList.get(position).getName());
        holder.tvDeskripsi.setText(mList.get(position).getDescription());
        Picasso.get().load(mList.get(position).getIcon()).into(holder.ivIcon);
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        @BindView(R.id.item_case_ivIcon)
        ImageView ivIcon;
        @BindView(R.id.item_case_tvTitle)
        TextView tvTitle;
        @BindView(R.id.item_case_tvDeskripsi)
        TextView tvDeskripsi;

        private int idCase;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @OnClick(R.id.item_case_cv) void caseClicked(){
            if(idCase == 3) {
                DialogBuilder.alertDialog(ctx, "Bear", "Maaf fitur belum tersedia");
            }else if (idCase == 2){
                Intent intent =  new Intent(ctx, CopingActivity.class);
                intent.putExtra("idCoping", 1);
                ctx.startActivity(intent);
            }else {
                Intent intent = new Intent(ctx, SymptomActivity.class);
                intent.putExtra("idCase", idCase);
                ctx.startActivity(intent);
            }
        }
    }

    public void generate(List<Case> list){
        clear();
        this.mList = list;
        notifyDataSetChanged();
    }

    public void clear(){
        this.mList.clear();
        notifyDataSetChanged();
    }
}
