package com.imaisnaini.bear.ui.view;

import com.imaisnaini.bear.db.model.History;

import java.util.List;

public interface HistoryView {
    void showLoading();
    void hideLoading();
    void loadHistory(List<History> historyList);
}
