package com.imaisnaini.bear.ui.dialog;


import android.content.Context;
import android.text.InputType;

import com.afollestad.materialdialogs.MaterialDialog;
import com.imaisnaini.bear.R;

public class DialogBuilder {

    public static MaterialDialog showLoadingDialog(Context ctx, String title, String content, boolean isCircularProgress) {
        MaterialDialog.Builder dialog = new MaterialDialog.Builder(ctx)
                .title(title)
                .content(content)
                .progress(true, 0)
                .progressIndeterminateStyle(isCircularProgress)
                .canceledOnTouchOutside(false);
        return dialog.show();
    }

    public static void showErrorDialog(Context ctx, String content) {
        new MaterialDialog.Builder(ctx)
                .title("Error")
                .content(content)
                .positiveText("OK")
                .show();
    }

    public static MaterialDialog showInputDialog(final Context context, String title, int resHint, MaterialDialog.InputCallback callback){
        MaterialDialog.Builder builder = new MaterialDialog.Builder(context)
                .title(title)
                .inputType(InputType.TYPE_CLASS_TEXT)
                .input(resHint, 0, false, callback);
        return builder.show();
    }

    public static MaterialDialog showInputDialog(final Context context, int title, int resHint, MaterialDialog.InputCallback callback){
        MaterialDialog.Builder builder = new MaterialDialog.Builder(context)
                .title(title)
                .inputType(InputType.TYPE_CLASS_TEXT)
                .input(resHint, 0, false, callback);
        return builder.show();
    }

    public static MaterialDialog closeDialog(final Context ctx, MaterialDialog.SingleButtonCallback callback){
        MaterialDialog.Builder builder = new MaterialDialog.Builder(ctx)
                .title(R.string.dialog_close_title)
                .content(R.string.dialog_close_content)
                .positiveText("Ya")
                .negativeText("Tidak")
                .onPositive(callback)
                .onNegative((dialog, which) -> dialog.dismiss());
        return builder.show();
    }

    public static MaterialDialog alertDialog(final Context ctx, String title, String content, MaterialDialog.SingleButtonCallback callback){
        MaterialDialog.Builder builder = new MaterialDialog.Builder(ctx)
                .title(title)
                .content(content)
                .positiveText("Ya")
                .negativeText("Tidak")
                .onPositive(callback)
                .onNegative((dialog, which) -> dialog.dismiss());
        return builder.show();
    }

    public static MaterialDialog alertDialog(final Context ctx, String title, String content){
        MaterialDialog.Builder builder = new MaterialDialog.Builder(ctx)
                .title(title)
                .content(content)
                .positiveText("OK")
                .onAny((dialog, which) -> dialog.dismiss());
        return builder.show();
    }

}
