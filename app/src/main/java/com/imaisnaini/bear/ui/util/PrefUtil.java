package com.imaisnaini.bear.ui.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.google.gson.Gson;

import java.util.HashSet;
import java.util.Set;

public class PrefUtil {
  public static final String COPING_SESSION = "coping_session";
  public static final String COPING_STEP = "index_step";

  public static SharedPreferences getSharedPreferences(Context ctx){
    return PreferenceManager.getDefaultSharedPreferences(ctx);
  }

  public static void putString(Context ctx, String key, String value){
    getSharedPreferences(ctx).edit().putString(key, value).apply();
  }

  public static String getString(Context ctx, String key){
    return getSharedPreferences(ctx).getString(key, null);
  }

  public static void putStringSet(Context ctx, String key, HashSet<String> value){
    getSharedPreferences(ctx).edit().putStringSet(key, value).apply();
  }

  public static Set<String> getStringSet(Context ctx, String key){
    return getSharedPreferences(ctx).getStringSet(key, null);
  }

  public static void putInt(Context ctx, String key, Integer value){
      getSharedPreferences(ctx).edit().putFloat(key, value).apply();
  }

  public static Integer getInt(Context ctx, String key){
      return getSharedPreferences(ctx).getInt(key, 0);
  }

  public static void clear(Context ctx) {
    getSharedPreferences(ctx).edit().clear().apply();
  }
}
