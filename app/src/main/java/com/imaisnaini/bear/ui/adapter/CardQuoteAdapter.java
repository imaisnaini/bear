package com.imaisnaini.bear.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.imaisnaini.bear.R;
import com.imaisnaini.bear.db.model.Quote;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CardQuoteAdapter extends RecyclerView.Adapter<CardQuoteAdapter.ViewHolder> {
    private Context context;
    private List<Quote> list = new ArrayList<>();

    public CardQuoteAdapter(Context context) {
        this.context = context;
    }

    public CardQuoteAdapter(Context context, List<Quote> list) {
        this.context = context;
        this.list = list;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_quote, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.tvQuote.setText(list.get(position).getQuote());
        if (list.get(position).getAuthor().isEmpty()){
            holder.tvAuthor.setVisibility(View.GONE);
        }else {
            holder.tvAuthor.setVisibility(View.VISIBLE);
            holder.tvAuthor.setText(list.get(position).getAuthor());
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder{
        @BindView(R.id.tvQuote_itemQuote)
        TextView tvQuote;
        @BindView(R.id.tvAuthor_itemQuote)
        TextView tvAuthor;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public void clear(){
        list.clear();
        notifyDataSetChanged();
    }

    public void generate(List<Quote> list){
        clear();
        this.list = list;
        notifyDataSetChanged();
    }
}
