package com.imaisnaini.bear.ui.fragment;


import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;

import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.airbnb.lottie.LottieAnimationView;
import com.imaisnaini.bear.R;
import com.imaisnaini.bear.ui.activity.MainActivity;
import com.imaisnaini.bear.ui.activity.SymptomActivity;
import com.imaisnaini.bear.ui.dialog.DialogBuilder;
import com.imaisnaini.bear.ui.util.PrefUtil;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * A simple {@link Fragment} subclass.
 */
public class CalmBreathingFragment extends Fragment {
    @BindView(R.id.tvInstruction_calmBreathing)
    TextView tvInstruction;
    @BindView(R.id.btnAction_calmBreathing)
    Button btnAction;
    @BindView(R.id.lavCalmBreathing)
    LottieAnimationView lavAnim;
    @BindView(R.id.progressBar_calmBreathing)
    ProgressBar progressBar;

    private MaterialDialog mDialog;
    private boolean isPause = false;
    private boolean flag = false;
    private ObjectAnimator animator;
    private int i = 0;
    private int j = 1;

    public CalmBreathingFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_calm_breathing, container, false);
        ButterKnife.bind(this, view);
        initViews();
        return view;
    }

    private void initViews(){
        tvInstruction.setText("Fokus pada pengendalian pernafasan sambil ikuti apa yang muncul dilayar. \n" +
                "Kita akan menghitung mundur dalam periode 1 menit");
        animator = ObjectAnimator.ofInt(progressBar, "progress", 0, 100);
        animator.setDuration(5000);
        animator.setInterpolator(new DecelerateInterpolator());
        animator.setRepeatCount(15);
        animator.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {
                tvInstruction.setText("Tarik nafas.");
            }

            @Override
            public void onAnimationEnd(Animator animator) {
                lavAnim.pauseAnimation();
                new Handler().postDelayed(() -> {
                    dialogFeedback();
                }, 3000);
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {
                i++;
                if (i % 2 == 0){
                    if (i < 3){
                        tvInstruction.setText("Tarik nafas.");
                    }else if (i < 7){
                        tvInstruction.setText("Tarik nafas, dan pikirkan angka '" + j + "'");
                        j++;
                    }else {
                        tvInstruction.setText(String.valueOf(j));
                        j++;
                    }
                }else {
                    if (i < 4){
                        tvInstruction.setText("Hembuskan.");
                    }else if(i < 8){
                        tvInstruction.setText("Hembuskan perlahan, dan pikirkan kata 'Relax'");
                    }else {
                        tvInstruction.setText("Relax.");
                    }
                }
            }
        });
    }

    @OnClick(R.id.btnAction_calmBreathing) void onClick(){
        if (flag){
            flag = false;
            btnAction.setText("Play");
            lavAnim.pauseAnimation();
            animator.pause();
        }else {
            flag = true;
            btnAction.setText("Pause");
            if (isPause) {
                lavAnim.resumeAnimation();
                animator.resume();
            }else {
                isPause = true;
                lavAnim.playAnimation();
                animator.start();
            }
        }
    }

    @OnClick(R.id.ivButtonClose_calmBreathing) void onClose(){
        DialogBuilder.closeDialog(getActivity(), (dialog, which) -> {
            startActivity(new Intent(getContext(), MainActivity.class));
            getActivity().finish();
        });
    }

    private void dialogFeedback(){
        mDialog = new MaterialDialog.Builder(getContext())
                .customView(R.layout.dialog_feedback, true)
                .backgroundColor(getResources().getColor(R.color.primaryLightColor))
                .build();

        TextView tvCoping = (TextView)mDialog.findViewById(R.id.tvCoping_dialogFeedback);
        CardView cvPositive = (CardView)mDialog.findViewById(R.id.cvPositive_dialogFeedback);
        CardView cvNeutral = (CardView)mDialog.findViewById(R.id.cvNeutral_dialogFeedback);
        CardView cvNegative = (CardView)mDialog.findViewById(R.id.cvNegative_dialogFeedback);

        tvCoping.setText("Calm Breathing");
        cvPositive.setOnClickListener(view -> {
            startActivity(new Intent(getContext(), MainActivity.class));
            getActivity().finish();
        });

        cvNeutral.setOnClickListener(view -> {
            isPause = false;
            flag = false;
            btnAction.setText("Start");
            tvInstruction.setText("Fokus pada pengendalian pernafasan sambil ikuti apa yang muncul dilayar. \n" +
                    "Kita akan menghitung mundur dalam periode 1 menit");
            i = 0;
            j = 1;
            mDialog.dismiss();
        });

        cvNegative.setOnClickListener(view -> {
            PrefUtil.clear(getActivity());
            Intent intent = new Intent(getContext(), SymptomActivity.class);
            intent.putExtra("idCase", 2);
            getActivity().startActivity(intent);
        });
        mDialog.show();
    }

}
