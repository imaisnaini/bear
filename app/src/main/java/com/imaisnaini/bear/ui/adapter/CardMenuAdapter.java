package com.imaisnaini.bear.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.imaisnaini.bear.R;
import com.imaisnaini.bear.db.model.Card;
import com.imaisnaini.bear.event.CardEvent;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CardMenuAdapter extends RecyclerView.Adapter<CardMenuAdapter.ViewHolder> {
    private Context context;
    private List<Card> list = new ArrayList<>();

    public CardMenuAdapter(Context context, List<Card> list) {
        this.context = context;
        this.list = list;
    }

    public CardMenuAdapter(Context context) {
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_card, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.tvTitle.setText(list.get(position).getName());
        holder.tvAuthor.setText(list.get(position).getAuthor());
        holder.cvLayout.setOnClickListener(view -> {
            EventBus.getDefault().postSticky(new CardEvent(list.get(position).getId()));
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder{
        @BindView(R.id.tvTitle_itemCard)
        TextView tvTitle;
        @BindView(R.id.tvAuthor_itemCard)
        TextView tvAuthor;
        @BindView(R.id.cvLayout_itemCard)
        CardView cvLayout;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public void generate(List<Card> list){
        clear();
        this.list = list;
        notifyDataSetChanged();
    }

    public void clear(){
        this.list.clear();
        notifyDataSetChanged();
    }
}
